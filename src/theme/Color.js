export const Colors = {
  Join_Btn: "#FF7272",
  ErrorColor: "#F14436",
  Light_Text: "#8E8E8E",
  Black_Color: "#3A3A3A",
  white_Color: "#FFFFFF",
  FooterColor: "#F7F7F7",
  BorderColor: "#D5D5D5",
  BorderColor: "#C5C5C5",
  Primary_Color: "#0099DC",
  OrderCodeColor: "#676767",
  Payment_Btn: "#FF0000",
};
