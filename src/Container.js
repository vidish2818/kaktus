import { store, persistor } from "./redux/Store";
import { Provider } from "react-redux";
import React, { Component } from "react";
import Navigation from "./navigation/Navigation";
import SplashScreen from "react-native-splash-screen";
import { PersistGate } from "redux-persist/integration/react";

export class Container extends Component {
  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide();
    }, 300);
  }
  render() {
    return (
      <>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Navigation />
          </PersistGate>
        </Provider>
      </>
    );
  }
}

export default Container;
