import React, { Component } from "react";
import { AppStack, AuthStack } from "./StackNavigator";
import { NavigationContainer } from "@react-navigation/native";
import { NavigationService } from "./index";
import { createStackNavigator } from "@react-navigation/stack";
import AsyncStorage from "@react-native-community/async-storage";

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
  }

  render() {
    const Stack = createStackNavigator();
    return (
      <>
        <NavigationContainer
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        >
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
            initialRouteName={"AuthStack"}
          >
            <Stack.Screen name="AuthStack" component={AuthStack} />
            <Stack.Screen name="AppStack" component={AppStack} />
          </Stack.Navigator>
        </NavigationContainer>
      </>
    );
  }
}

export default Navigation;
