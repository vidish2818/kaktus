import React from "react";
import { BottomTab } from "./BottomTabs";
import Signin from "../screens/Signin/Signin";
import Receipt from "../screens/Receipt/Receipt";
import GetStarted from "../screens/GetStarted/GetStarted";
import { createStackNavigator } from "@react-navigation/stack";
import SignaturePad from "../screens/SignaturePad/SignaturePad";
import Splashscreen from "../screens/SplashScreen/SplashScreen";
import QrCodeScreen from "../screens/QrCodeScanner/QrCodeScreen";
import DeliverDetails from "../screens/DeliverDetails/DeliverDetails";
import ServicesData from "../screens/ServicesData/ServicesData";

const Stack = createStackNavigator();

export const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="SplashScreen" component={Splashscreen} />
      <Stack.Screen name="GetStarted" component={GetStarted} />
      <Stack.Screen name="Signin" component={Signin} />
    </Stack.Navigator>
  );
};

export const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="BottomTab"
    >
      <Stack.Screen name="BottomTab" component={BottomTab} />
      <Stack.Screen name="Qrcode" component={QrCodeScreen} />
      <Stack.Screen name="DeliveryDetails" component={DeliverDetails} />
      <Stack.Screen name="Receipt" component={Receipt} />
      <Stack.Screen name="SignaturePad" component={SignaturePad} />
      <Stack.Screen name="ServicesData" component={ServicesData} />
    </Stack.Navigator>
  );
};
