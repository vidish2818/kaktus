import React from "react";
import {
  HomeBottomIcon,
  DeliverBottomIcon,
  ReturnBottomIcon,
  TeleHealthBottomIcon,
  ChatBottomIcon,
  SettingBottomIcon,
  DeactivateHome,
  DeactivateDeliver,
  DeactivateReturn,
  DeactivateTeleHealth,
  DeactivateChat,
  DeactivateSetting,
} from "../assets/index";
import { Colors } from "../theme";
import { RFontSize } from "../utils";
import { I18n, FontSize, Fonts } from "../constant";
import { Text, Image, View, StyleSheet } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../screens/Home/Home";
import Deliver from "../screens/Deliver/Deliver";
import Return from "../screens/Return/Return";
import TeleHealth from "../screens/TeleHealth/TeleHealth";
import Chatting from "../screens/Chatting/Chatting";
import Settings from "../screens/Settings/Settings";

export const BottomTab = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let imgUrl;
          let tabTitle;
          if (route.name === "Home") {
            imgUrl = focused ? DeactivateHome : HomeBottomIcon;
            tabTitle = I18n.t("tab1");
          } else if (route.name === "Deliver") {
            imgUrl = focused ? DeactivateDeliver : DeliverBottomIcon;
            tabTitle = I18n.t("tab2");
          } else if (route.name === "Return") {
            imgUrl = focused ? DeactivateReturn : ReturnBottomIcon;
            tabTitle = I18n.t("tab3");
          } else if (route.name === "Telehealth") {
            imgUrl = focused ? DeactivateTeleHealth : TeleHealthBottomIcon;
            tabTitle = I18n.t("tab4");
          } else if (route.name === "Chat") {
            imgUrl = focused ? DeactivateChat : ChatBottomIcon;
            tabTitle = I18n.t("tab5");
          } else if (route.name === "Setting") {
            imgUrl = focused ? DeactivateSetting : SettingBottomIcon;
            tabTitle = I18n.t("tab6");
          }
          return (
            <View>
              <Image source={imgUrl} style={styles.tabsImages} />
              <Text numberOfLines={1} style={styles.tabsLabel}>
                {tabTitle}
              </Text>
            </View>
          );
        },
      })}
      tabBarOptions={{
        showLabel: false,
        style: styles.bottomTabStyle,
        inactiveTintColor: Colors.Light_Text,
        activeTintColor: Colors.Primary_Color,
      }}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Deliver" component={Deliver} />
      <Tab.Screen name="Return" component={Return} />
      <Tab.Screen name="Telehealth" component={TeleHealth} />
      <Tab.Screen name="Chat" component={Chatting} />
      <Tab.Screen name="Setting" component={Settings} />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  tabsLabel: {
    marginTop: 10,
    width: "100%",
    textAlign: "center",
    fontSize: FontSize.font12,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  tabsImages: {
    marginTop: 5,
    alignSelf: "center",
    width: RFontSize(25),
    height: RFontSize(25),
    resizeMode: "contain",
  },
  bottomTabStyle: {
    width: "100%",
    alignSelf: "center",
    height: RFontSize(90),
    backgroundColor: Colors.white_Color,
  },
});
