import { StyleSheet } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";

export default StyleSheet.create({
  container: {
    padding: 20,
    width: "90%",
    height: "auto",
    marginTop: "5%",
    borderRadius: 20,
    alignSelf: "center",
    backgroundColor: Colors.white_Color,
  },
  firstSetion: {
    paddingVertical: 10,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: Colors.BorderColor,
    justifyContent: "space-between",
  },
  lightText: {
    color: Colors.OrderCodeColor,
  },
  mainText: {
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontRegular,
  },
  subText: {
    fontSize: FontSize.font10,
    fontFamily: Fonts.fontRegular,
  },
  secondSection: {
    flexDirection: "row",
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: Colors.BorderColor,
    justifyContent: "space-between",
  },
  extraStyles: {
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    borderColor: Colors.BorderColor,
    justifyContent: "space-between",
  },
  orderRxText: {
    paddingBottom: 3,
    fontSize: FontSize.font10,
    color: Colors.OrderCodeColor,
    fontFamily: Fonts.fontRegular,
  },
  orderNameText: {
    paddingBottom: 3,
    fontSize: FontSize.font13,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  orderCodeText: {
    paddingBottom: 3,
    fontSize: FontSize.font10,
    color: Colors.OrderCodeColor,
    fontFamily: Fonts.fontRegular,
  },
  priceText: {
    fontSize: FontSize.font13,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  totalOrderAmountSection: {
    paddingVertical: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  arrowSection: {
    width: "30%",
    flexDirection: "row",
    alignItems: "center",
  },
  accordianLogo: {
    width: RFontSize(25),
    height: RFontSize(25),
    resizeMode: "contain",
  },
  arrowValue: {
    paddingLeft: 10,
    fontSize: FontSize.font10,
    color: Colors.Light_Text,
    fontFamily: Fonts.fontRegular,
  },
  totalAmountSection: {
    width: "70%",
    justifyContent: "center",
    alignItems: "flex-end",
  },
  amountDetails: {
    fontSize: FontSize.font13,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  allOrdersSection: {
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderColor: Colors.BorderColor,
  },
  noRecordText: {
    textAlign: "center",
    fontSize: FontSize.font14,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  orderMsg: {
    width: "100%",
    alignSelf: "center",
    paddingVertical: 10,
    borderBottomWidth: 0.5,
  },
});
