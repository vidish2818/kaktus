import React from "react";
import styles from "./AllOrdersListBoxStyles";
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import { DownArrow, UpArrow } from "../../assets";

const AllOrdersListBox = (props) => {
  const {
    code,
    lengthOfItem,
    listOfOrders,
    dataShow,
    arrowOnpress,
    totalPrice,
    paid,
    due,
    arrowWiseValue,
    displayArrow,
  } = props;
  return (
    <View style={[styles.container, styles.shadow]}>
      <View style={styles.firstSetion}>
        <Text style={[styles.mainText, styles.lightText]}>
          Order ID: {code}
        </Text>
        <Text style={[styles.subText, styles.lightText]}>
          {lengthOfItem} Items in this order
        </Text>
      </View>
      <View style={styles.secondSection}>
        <Text style={[styles.subText, styles.lightText]}>Info/Code</Text>
        <Text style={[styles.subText, styles.lightText]}>Amount</Text>
      </View>
      {listOfOrders.length > 0 ? (
        <FlatList
          style={styles.allOrdersSection}
          data={listOfOrders}
          renderItem={({ item, index }) => {
            return (
              <View
                style={[
                  styles.extraStyles,
                  {
                    borderBottomWidth:
                      listOfOrders.length - 1 === index ? 0 : 1,
                  },
                ]}
              >
                <View>
                  {dataShow && (
                    <Text style={styles.orderRxText}>{item.RxNo}</Text>
                  )}
                  <Text style={styles.orderNameText}>{item.DrugName}</Text>
                  {dataShow && (
                    <Text style={styles.orderCodeText}>{item.NDCCode}</Text>
                  )}
                </View>
                <View>
                  <Text style={styles.priceText}>{`$${item.Price}`}</Text>
                </View>
              </View>
            );
          }}
        />
      ) : (
        <View style={styles.orderMsg}>
          <Text style={styles.noRecordText}>No orders are available!!</Text>
        </View>
      )}
      <View style={styles.totalOrderAmountSection}>
        <View style={styles.arrowSection}>
          <TouchableOpacity onPress={arrowOnpress}>
            {displayArrow && (
              <Image
                source={!dataShow ? DownArrow : UpArrow}
                style={styles.accordianLogo}
              />
            )}
          </TouchableOpacity>
          <Text style={styles.arrowValue}> {arrowWiseValue}</Text>
        </View>
        <View style={styles.totalAmountSection}>
          <Text style={styles.amountDetails}>Total: {`${totalPrice}`}</Text>
          <Text style={styles.amountDetails}>Paid: {`${paid}`}</Text>
          <Text style={styles.amountDetails}>Due: {`${due}`}</Text>
        </View>
      </View>
    </View>
  );
};

export default AllOrdersListBox;
