import {StyleSheet} from 'react-native';
import { Colors } from '../../theme';

export default StyleSheet.create({
  scrn_root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#00000040',
    justifyContent: 'space-around',
  },
  activityIndicatorWrapper: {
    width: 130,
    height: 'auto',
    display: 'flex',
    paddingVertical: 15,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: Colors.Black_Color,
    justifyContent: 'space-around',
  },
  loading_txt: {
    fontSize: 16,
    fontWeight: '600',
    textAlign: 'center',
    color: Colors.white_Color
  },
});
