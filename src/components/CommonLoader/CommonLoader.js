import React, {useEffect} from 'react';
import styles from './CommonLoaderStyles';
import {Round_Bg, KaktusLogoSS} from '../../assets';
import {View, Modal, Text, Animated, Easing} from 'react-native';
import { Colors } from '../../theme';
import { Spinner } from 'native-base';

export const CommonLoader = (props) => {
  const {loading, ...attributes} = props;
  // spinValue = new Animated.Value(0);

  // const spin = () => {
  //   spinValue.setValue(0);
  //   Animated.timing(spinValue, {
  //     toValue: 5,
  //     duration: 4000,
  //     easing: Easing.linear,
  //   }).start();
  // };

  // useEffect(() => {
  //   spin();
  // }, []);

  // const spinner = spinValue.interpolate({
  //   inputRange: [0, 1],
  //   outputRange: ['0deg', '360deg'],
  // });

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {
        console.log('close modal');
      }}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          {/* <Animated.Image
            style={[
              {
                width: RFontSize(80),
                height: RFontSize(80),
                resizeMode: 'contain',
                marginBottom: 10,
                transform: [{rotate: spinner}],
              },
            ]}
            source={Round_Bg}
          /> */}
          <Spinner color={Colors.white_Color}   />
          <Text style={styles.loading_txt}>Loading...</Text>
        </View>
      </View>
    </Modal>
  );
};

export default CommonLoader;
