import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text,
} from "react-native";
import { RFontSize } from "../../utils";
import styles from "./CommonbackgroundStyles";
import { Round_Bg, Backarrow } from "../../assets";
import { connect } from "react-redux";
import { useNavigation } from "@react-navigation/native";

const Commonbackground = (props) => {

  const {
    style,
    cartBtn,
    mainLogo,
    backPress,
    addtocart,
    cartLength,
    BackarrowVisible,
    CartIconVisible,
    logoStyle,
  } = props;
  const lengthOfCartItems = props.cartLength?.allCartItems?.length;
  const navigation = useNavigation();

  return (
    <View style={[styles.container, style]}>
      <ImageBackground source={Round_Bg} resizeMode="cover" style={styles.bg} />
      <View style={styles.header}>
        {BackarrowVisible ? (
          <TouchableOpacity onPress={backPress} style={styles.firstIcon}>
            <Image
              source={Backarrow}
              style={{
                resizeMode: "contain",
                width: RFontSize(30),
                height: RFontSize(30),
              }}
            />
          </TouchableOpacity>
        ) : null}
        {CartIconVisible ? (
          <TouchableOpacity
            onPress={() => navigation.navigate("Deliver")}
            style={styles.secondIcon}
          >
            <Image
              source={cartBtn}
              style={{
                resizeMode: "contain",
                width: RFontSize(30),
                height: RFontSize(25),
              }}
            />
            {lengthOfCartItems != undefined && lengthOfCartItems != 0 && (
              <Text style={styles.cartBoxText}>{lengthOfCartItems}</Text>
            )}
          </TouchableOpacity>
        ) : null}
      </View>
      <View style={[{ alignSelf: "center" }, logoStyle]}>
        <Image
          // animation="fadeInDown"
          // duration={1000}
          source={mainLogo}
          style={{
            resizeMode: "contain",
            width: RFontSize(171),
            height: RFontSize(82),
          }}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    cartLength: state.CartItems,
  };
};

export default connect(mapStateToProps, null)(Commonbackground);