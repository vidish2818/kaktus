import { Colors } from "../../theme";
import { RFontSize } from "../../utils";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.Primary_Color,
  },
  header: {
    width: "95%",
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  firstIcon: {
    width: "50%",
  },
  secondIcon: {
    width: "50%",
    alignItems: "flex-end",
  },
  bg: {
    top: 0,
    right: -80,
    position: "absolute",
    width: RFontSize(356),
    height: RFontSize(324),
  },
  cartBoxText: {
    borderWidth: 1,
    textAlign: "center",
    borderRadius: 10,
    position: "absolute",
    top: 0,
    right: 0,
    height: 'auto',
    width: RFontSize(16),
    color: Colors.white_Color,
    backgroundColor: Colors.ErrorColor,
    overflow: "hidden",
    borderColor: Colors.ErrorColor,
    fontSize: 10,
  },
});
