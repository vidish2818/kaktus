import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
  MainBtn: {
    width: wp('100%'),
    paddingVertical: 12,
  },
});
