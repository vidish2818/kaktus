import React from 'react';
import {Colors} from '../../theme';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './CommonButtonStyles';
import Ripple from 'react-native-material-ripple';

const CommonButton = (props) => {
  const {btnName, style, textStyle, onPress} = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.4}
      // rippleOpacity={0.54}
      // rippleDuration={2400}
      // rippleColor={Colors.white_Color}
      style={[styles.MainBtn, style]}>
      <Text style={textStyle}>{btnName}</Text>
    </TouchableOpacity>
  );
};

export default CommonButton;
