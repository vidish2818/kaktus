import React from 'react';
import {View, TextInput, Image, TouchableOpacity} from 'react-native';
import styles from './CommonInputStyles';

const CommonInput = (props) => {
  const {userIcon, eyeIcon, eyeOnpress,userVisible, lockVisible, inputStyle} = props;
  return (
    <View style={[styles.container, inputStyle]}>
      {userVisible ? <TouchableOpacity style={styles.firstSection}>
        <Image source={userIcon} style={styles.icons} />
      </TouchableOpacity> : null}
      <View style={styles.secondSection}>
        <TextInput {...props} style={styles.textinputs}/>
      </View>
      {lockVisible ? <TouchableOpacity style={styles.thirdSection} onPress={eyeOnpress}>
        <Image source={eyeIcon} style={styles.icons} />
      </TouchableOpacity> : null}
    </View>
  );
};

export default CommonInput;
