import {Colors} from '../../theme';
import {Fonts,FontSize} from '../../constant';
import {RFontSize} from '../../utils';
import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    justifyContent: 'center',
    borderBottomColor: Colors.BorderColor,
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  firstSection: {
    width: '10%',
  },
  secondSection: {
    width: '80%',
    justifyContent: 'center',
  },
  thirdSection: {
    width: '10%',
    alignItems: 'center',
  },
  icons: {
    height: RFontSize(22),
    width: RFontSize(22),
    resizeMode: 'contain',
  },
  textinputs: {
    fontSize: FontSize.font14,
    fontFamily: Fonts.fontRegular,
  },
});
