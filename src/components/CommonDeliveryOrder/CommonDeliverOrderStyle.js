import { StyleSheet, Dimensions } from "react-native";
import {Fonts, FontSize} from '../../constant';
import {Colors} from '../../theme';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';

const {height} = Dimensions.get('window');

export default StyleSheet.create({
  otherServices: {
    height: hp("8%"),
    width: wp("80%"),
    paddingHorizontal: 15,
    marginTop: "5%",
    borderRadius: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: Colors.white_Color,
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  firstSection: {
    justifyContent: "center",
  },
  mainName: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  subName: {
    fontSize: FontSize.font12,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  secondSection: { justifyContent: "center" },
});
