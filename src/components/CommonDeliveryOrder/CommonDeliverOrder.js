import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { ForwardArrow } from "../../assets/index";
import styles from "./CommonDeliverOrderStyle";
import { I18n } from "../../constant";

const CommonDeliverOrder = (props) => {
  const { ServiceName, onPress, arrowforword } = props;
  return (
    <TouchableOpacity
      style={[styles.otherServices, styles.shadow]}
      onPress={onPress}
    >
      <View style={styles.firstSection}>
        <View>
          <Text style={styles.mainName}>{ServiceName}</Text>
        </View>
        <View>
          <Text style={styles.subName}>{I18n.t("seeOrder")}</Text>
        </View>
      </View>
      <View style={styles.secondSection}>
        <Image source={ForwardArrow} style={{ resizeMode: "contain" }} />
      </View>
    </TouchableOpacity>
  );
};

export default CommonDeliverOrder;
