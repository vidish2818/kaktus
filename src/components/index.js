import CommonInput from './CommonInput/CommonInput';
import CommonButton from './CommonButton/CommonButton';
import CommonLoader from './CommonLoader/CommonLoader';
import Commonbackground from './Commonbackground/Commonbackground';
import CommonAddressBox from './CommonAddressBox/CommonAddressBox';
import AllOrdersListBox from './AllOrdersListBox/AllOrdersListBox';
import CommonDeliverOrder from './CommonDeliveryOrder/CommonDeliverOrder'

export {
  CommonInput,
  CommonButton,
  CommonLoader,
  AllOrdersListBox,
  Commonbackground,
  CommonAddressBox,
  CommonDeliverOrder
};
