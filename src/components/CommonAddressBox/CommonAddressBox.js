import React from "react";
import { View, Text } from "react-native";
import styles from "./CommonAddressBoxStyles";

const CommonAddressBox = (props) => {
  const {
    name,
    style,
    address1,
    address2,
    addressDetails,
    deliveryInstruction,
  } = props;
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.mainText}>
        Patient Name : <Text style={styles.fullName}>{name}</Text>
      </Text>
      {address1 ? (
        <Text style={styles.addressDetailsStyle}>{address1}</Text>
      ) : null}
      {address2 ? (
        <Text style={styles.addressDetailsStyle}>{address2}</Text>
      ) : null}
      {addressDetails ? (
        <Text style={styles.addressDetailsStyle}>{addressDetails}</Text>
      ) : null}
      {deliveryInstruction ? (
        <Text style={styles.addressDetailsStyle}>
          Delivery Instruction : {deliveryInstruction}
        </Text>
      ) : null}
    </View>
  );
};

export default CommonAddressBox;
