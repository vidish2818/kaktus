import {StyleSheet} from 'react-native';
import {Fonts,FontSize} from '../../constant';
import {Colors} from '../../theme';

export default StyleSheet.create({
  container: {
    padding: 20,
    width: '100%',
    height: 'auto',
    borderRadius: 20,
    alignSelf: 'center',
    backgroundColor: Colors.white_Color,
  },
  mainText: {
    paddingBottom: 2,
    fontSize: FontSize.font14,
    color: Colors.Light_Text,
    fontFamily: Fonts.fontMedium,
  },
  fullName: {
    paddingBottom: 2,
    fontSize: FontSize.font14,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  addressDetailsStyle: {
    paddingBottom: 3,    
    fontSize: FontSize.font10,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
});
