import {StyleSheet, Dimensions} from 'react-native';
import {Fonts, FontSize} from '../../constant';
import {Colors} from '../../theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { RFontSize } from '../../utils';

const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 0.8,
  },
  footerView: {
    flex: 5.2,
    height: height,
    paddingTop: 3,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  headerView: {
    width: '90%',
    marginTop: '8%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  headerText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  headerText2: {
    fontSize: FontSize.font12,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  addressSection: {
    width: '90%',
    alignSelf: 'center',
    marginTop: '5%',
  },
  shadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  btns: {
    borderRadius: 50,
    marginBottom: '5%',
    justifyContent: 'center',
    // paddingVertical: 13,
  },
  btnView: {
    width: "90%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
  },
  collectPaymentBtn: {
    width: '45%',
    marginTop: '10%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: Colors.Join_Btn,
  },
  collectPaymentBtn2: {
    width: '45%',
    marginTop: '10%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: 'green',
  },
  signatureBtn: {
    width: '45%',
    marginTop: '10%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: Colors.Primary_Color,
  },
  ConsultBtn: {
    width: '45%',
    marginTop: '5%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: Colors.Join_Btn,
  },
  ConsultBtn2: {
    width: '45%',
    marginTop: '5%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: 'green',
  },
  CompleteBtn: {
    width: '45%',
    marginTop: '5%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: Colors.Join_Btn,
  },
  btnsTexts: {
    textAlign: 'center',
    fontSize: FontSize.font13,
    fontFamily: Fonts.fontMedium,
    color: Colors.white_Color,
  },
  flatlistWrapper: {
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'space-between',
  },
  bottomView: {
    marginTop: '10%',
    alignSelf: 'center',
    paddingBottom: 10
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: 'contain',
  }
});
