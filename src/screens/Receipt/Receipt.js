import React, { Component } from "react";
import styles from "./ReceiptStyles";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Dimensions,
  Image,
} from "react-native";
import {
  AllOrdersListBox,
  CommonAddressBox,
  Commonbackground,
  CommonButton,
  CommonInput,
  CommonLoader,
} from "../../components";
import { CartIcon, PoweredByLogo, Kaktus_Logo } from "../../assets";
import { Colors } from "../../theme";
import {getCartRecords} from '../../redux/Actions/getCartRecords'
import { connect } from "react-redux";
import { BASE_API_URL } from "../../config/config";
import AsyncStorage from '@react-native-community/async-storage'

const { height, width } = Dimensions.get("window");

export class Receipt extends Component {
  constructor(props) {
    super(props);
    const { route } = this.props;
    this.state = {
      allBtns: [
        { btnName: "Collect Payment" },
        { btnName: "Signature" },
        { btnName: "Consult" },
      ],
      dataShown: true,
      orderData: route?.params?.selectedOrder,
      cartRemoveArray: [],
      userToken: "",
      isLoading: false
    };
  }

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  signature = () => {
    this.props.navigation.navigate("SignaturePad", {
      data: this.state.orderData,
    });
  };

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userToken: data });
  }

  CompleteOrder = () => {
    const { userToken, orderData } = this.state;
    let token = userToken?.signInUserSession?.idToken?.jwtToken;
    let orderId = orderData?.OrderId;

    let orderUpdateComplete = {
      cartOrderId: orderId
    };
    try {
      this.setState({isLoading: true})
      fetch(`${BASE_API_URL}/order-by-code/update-cart`, {
        method: "PUT",
        withCredentials: true,
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(orderUpdateComplete),
      })
        .then((response) =>response.json())
        .then((res) => {
          setTimeout(() => {
            this.props.CompleteOrderData(token)
          }, 5000);
          this.setState({isLoading: false})
          this.props.navigation.navigate("Home")
        });
    } catch (error) {
      console.log('error', error);
    }
  };

  render() {
    const { allBtns, dataShown, orderData, isLoading } = this.state;
    let Payment = orderData?.amount?.isPaid;
    let isConsult = orderData?.isConsultRequired;
    return (
      <>
      <CommonLoader loading={isLoading}/>
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            backPress={this.goToBackScreen}
          />
          <View style={styles.footerView}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
              style={{ flex: 1, paddingBottom: 400 }}
            >
              <View style={styles.headerView}>
                <Text style={styles.headerText}>
                  {orderData?.patient?.PatientName}
                </Text>
                <Text style={styles.headerText2}>
                  PU Code: {orderData?.pucode}
                </Text>
              </View>

              <AllOrdersListBox
                code={orderData?.StockCode}
                lengthOfItem={orderData?.ItemsInThisOrder}
                listOfOrders={orderData?.order_details}
                dataShow={dataShown}
                totalPrice={orderData?.amount?.Total}
                paid={orderData?.amount?.Paid}
                due={orderData?.amount?.Due}
                displayArrow={false}
              />
              {/* <CommonAddressBox
                style={[styles.addressSection, styles.shadow]}
                name={orderData?.patient?.PatientName}
                address1={orderData?.patient?.Address?.AddressLine1}
                address2={orderData?.patient?.Address?.AddressLine2}
                addressDetails={`${orderData?.patient?.Address?.City}, ${orderData?.patient?.Address?.State} - ${orderData?.patient?.Address?.ZipCode}`}
                // addressDetails={
                //   (orderData?.patient?.Address?.City,
                //   orderData?.patient?.Address?.State - orderData?.patient?.Address?.ZipCode)
                // }
                deliveryInstruction={orderData?.DeliveryInstruction}
              /> */}

              <View style={styles.btnView}>
                <CommonButton
                  btnName="Collect Payment"
                  style={
                    Payment === false
                      ? styles.collectPaymentBtn
                      : styles.collectPaymentBtn2
                  }
                  textStyle={styles.btnsTexts}
                />
                <CommonButton
                  btnName="Signature"
                  style={styles.signatureBtn}
                  textStyle={styles.btnsTexts}
                  onPress={this.signature}
                />
              </View>
              <View style={styles.btnView}>
                <CommonButton
                  btnName="Consult"
                  style={
                    isConsult === false ? styles.ConsultBtn : styles.ConsultBtn2
                  }
                  textStyle={styles.btnsTexts}
                />
                <CommonButton
                  btnName="Complete"
                  style={styles.CompleteBtn}
                  textStyle={styles.btnsTexts}
                  onPress={this.CompleteOrder}
                />
              </View>
              <View style={styles.bottomView}>
                <Image source={PoweredByLogo} style={styles.bottomContent} />
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    CompleteOrderData: (tokenData) => {
      dispatch(getCartRecords(tokenData));
    },
  };
};

export default connect(null, mapDispatchToProps)(Receipt);
