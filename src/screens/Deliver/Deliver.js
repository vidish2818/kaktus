import React, { Component } from "react";
import styles from "./DeliverStyles";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  Dimensions,
} from "react-native";
import { Commonbackground, CommonLoader } from "../../components";
import {
  EyeOn,
  ForwardArrow,
  CartIcon,
  PoweredByLogo,
  Kaktus_Logo,
} from "../../assets";
import { I18n } from "../../constant";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import { BASE_API_URL } from "../../config/config";

const { height, width } = Dimensions.get("window");

export class Deliver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      numberOfLines: 1,
      userToken: "",
      cartRecords: [],
      cartData: ""
    };
  }

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  gotoReceipt = (item) => {
    this.props.navigation.navigate("Receipt", { selectedOrder: item });
  };

  render() {
    const { isLoading, numberOfLines, cartRecords, cartData } = this.state;
    const {AllCartItem} = this.props;
    return (
      <>
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            backPress={this.goToBackScreen}
          />
          <View style={styles.footerView}>
            <View style={styles.headerView}>
              <Text style={styles.headerText}>{I18n.t("readyForDeliver")}</Text>
            </View>
            <View style={styles.headerView2}>
              <Text style={styles.headerText2}>{I18n.t("PendingOrders")}</Text>
            </View>
            {AllCartItem && AllCartItem?.allCartItems?.length > 0 ? (
              <FlatList
                data={AllCartItem.allCartItems}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                  return (
                    <View style={styles.ordersList}>
                      <View style={styles.firstView}>
                        <View style={styles.orderId}>
                          <Text style={styles.subText}>
                            Order ID : {item?.StockCode}
                          </Text>
                        </View>
                        <View style={styles.patientName}>
                          <Text style={styles.mainText} numberOfLines={1}>
                            {item?.patient?.PatientName}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.secondView}>
                        <View style={styles.drugName}>
                          {item?.order_details?.map((val, index) => {
                            return (
                              <Text
                                style={styles.orderdetail}
                                numberOfLines={1}
                              >
                                {`${val?.DrugName}, `}
                              </Text>
                            );
                          })}
                        </View>
                        <TouchableOpacity
                          style={styles.navigateIcon}
                          onPress={() => this.gotoReceipt(item)}
                        >
                          <Image
                            source={ForwardArrow}
                            style={styles.arrowImg}
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={styles.thirdView}>
                        {/* <View>
                              <Text>Hello</Text>
                            </View> */}
                        <View style={styles.courierName}>
                          <Text style={styles.pickuptext}>
                            {item?.patient?.location?.Name}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            ) : (
              <View style={{ alignSelf: "center", marginTop: "40%" }}>
                <Text style={styles.noRecordText}>No records found!</Text>
              </View>
              // <View>
              //   <Text>{saveDetail.Object}</Text>
              // </View>
            )}
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    AllCartItem: state.CartItems
  }
}

export default connect(mapStateToProps, null)(Deliver);

