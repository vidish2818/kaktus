import { StyleSheet, Dimensions } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { colors } from "react-native-elements";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.0,
  },
  footerView: {
    flex: 5.0,
    height: height,
    paddingTop: 3,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  headerView: {
    width: "90%",
    marginTop: "8%",
    alignSelf: "center",
    marginBottom: "5%",
  },
  headerView2: {
    width: "90%",
    alignSelf: "center",
    marginBottom: "5%",
  },
  headerText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  headerText2: {
    fontSize: FontSize.font16,
    color: Colors.Primary_Color,
    fontFamily: Fonts.fontMedium,
  },
  ordersList: {
    width: "90%",
    marginBottom: 5,
    paddingBottom: 5,
    alignSelf: "center",
    borderBottomWidth: 1,
  },
  firstView: {
    width: "90%",
    paddingVertical: 3,
    flexDirection: "row",
    alignItems: "center",
  },
  orderId: {
    width: "50%",
  },
  subText: {
    fontSize: FontSize.font12,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  patientName: {
    width: "50%",
  },
  mainText: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
    textAlign: "right",
  },
  secondView: {
    width: "100%",
    paddingVertical: 3,
    flexDirection: "row",
  },
  drugName: {
    width: "80%",
  },
  orderdetail: {
    flexWrap: "wrap",
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  navigateIcon: {
    width: "20%",
    justifyContent: "center",
  },
  arrowImg: {
    width: RFontSize(25),
    height: RFontSize(25),
    resizeMode: "contain",
    alignSelf: "flex-end",
  },
  thirdView: {
    width: "100%",
    paddingVertical: 3,
    flexDirection: "row",
  },
  courierName: {
    // marginLeft: 10
  },
  pickuptext: {
    fontSize: FontSize.font13,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  bottomView: {
    marginTop: "5%",
    alignSelf: "center",
  },
  bottomLogo: {
    bottom: 0,
    alignSelf: "center",
    position: "absolute",
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  fotTabletUi: {
    // width: '47%',
    // borderRadius: 15,
    // height: hp('16%'),
    // marginBottom: '5%',
    // alignSelf: 'center',
    // alignItems: 'center',
    // paddingHorizontal: 15,
    // justifyContent: 'center',
    // backgroundColor: Colors.white_Color,
    width: "90%",
    height: hp("10%"),
    marginBottom: "5%",
    borderBottomWidth: 1,
    alignSelf: "center",
    borderBottomColor: Colors.BorderColor,
  },
  noRecordText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
});
