import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import styles from "./SettingsStyles";
import AsyncStorage from "@react-native-community/async-storage";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { Commonbackground, CommonButton, CommonInput } from "../../components";
import {
  CartIcon,
  Kaktus_Logo,
  PoweredByLogo,
  UserPofileImage,
  DownArrow,
  UpArrow,
} from "../../assets/index";
import { Colors } from "../../theme";
import Toast from "react-native-simple-toast";
import MultiSelect from "react-native-multiple-select";
import { Fonts } from "../../constant";
import { TextInput } from "react-native";
import { BASE_API_URL, BASE_API_URL_UPDATE } from "../../config/config";
export class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userData: "",
      dataShow: false,
      show: false,
      visible: true,
      email: "",
      firstName: "",
      lastName: "",
      PhoneNumber: "",
      bedgeId: "",
      imageSource: null,
      selectedItems: [],
      roles: [],
      userProfileData: "",
      checked: []
    };
  }

  onSelectedItemsChange = (selectedItems) => {
    this.setState({ selectedItems });
  };

  _changeIcon = () => {
    const { show, visible } = this.state;
    this.setState({
      show: !show,
      visible: !visible,
    });
  };

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  signOut = () => {
    AsyncStorage.removeItem("user");
    this.props.navigation.navigate("AuthStack");
  };

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userData: data });
    this.getAccountDetail();
  }

  inputChangeHandler = (name) => (event) => {
    this.setState({
      [name]: event,
    });
  };

  option = {
    title: "Pick an Image",
    maxWidth: 256,
    maxHeight: 256,
    noData: true,
    mediaType: "photo",
    storageOption: {
      skipBackup: true,
      path: "image",
    },
  };

  UploadImage = () => {
    launchImageLibrary(this.option, (response) => {
      console.log({ response });
      if (response.didCancel) {
        Toast.show("You did not select any image", Toast.LONG);
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else{
        let source = { uri: response.uri };
        console.log({ source });

        this.setState({
          imageSource: source.uri,
        });
        // this.setState({imageSource:response})
      }
      // if(response.uri){
      //   this.setState({imageSource:response})
      // }
    });
  };

  getAccountDetail = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;

    fetch(`${BASE_API_URL_UPDATE}/users/${userData?.username}`, {
      method: "GET",
      withCredentials: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res) {
          this.setState({ userProfileData: res[0] });
          let rolesItems = res[0].roles.map((ele) => {
            return { id: ele.id, name: ele.name };
          });
          this.setState({
            roles: rolesItems,
            firstName: res[0].first_name,
            lastName: res[0].last_name,
            email: res[0].username,
            PhoneNumber: res[0].contact?.phone,
            bedgeId: res[0].badge_id,
            imageSource: res[0].picture_url,
          });
        }
      });
  };

  saveAccountDetail = () => {
    const { userData, userProfileData, roles } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    let updateAccountDetail = {
      id: userData?.username,
      file: this.state.imageSource,
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      badge_id: this.state.bedgeId,
      parent_account_id: userProfileData?.parent_account_id,
      roles: this.state.roles,
      phone: this.state.PhoneNumber
    };
    console.log('updateAccountDetail', updateAccountDetail);

  //   fetch(`${BASE_API_URL_UPDATE}/users`, {
  //     method: "POST",
  //     withCredentials: true,
  //     headers: {
  //       Authorization: "Bearer " + token,
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify(updateAccountDetail),
  //   })
  //     .then((response) => {
  //       return response.json();
  //     })
  //     .then((res) => {
  //       this.props.navigation.navigate("Home")
  //     });
  };

  render() {
    const {
      userData,
      dataShow,
      show,
      visible,
      email,
      firstName,
      lastName,
      PhoneNumber,
      bedgeId,
      imageSource,
      selectedItems,
      roles,
      userProfileData,
    } = this.state;
    let user = userData && userData.attributes && userData.attributes;
    return (
      <>
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            backPress={this.goToBackScreen}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
          />
          <View style={styles.footerView}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
              style={{ flex: 1, paddingBottom: 400 }}
            >
              <View style={styles.textview}>
                <View style={styles.profileUriImg} onPress={this.UploadImage}>
                  {imageSource === null ? (
                    <Image source={UserPofileImage} style={styles.profileImg} />
                  ) : (
                    <Image
                      source={{ uri: imageSource }}
                      style={styles.profileImg2}
                    />
                  )}
                  <TouchableOpacity onPress={this.UploadImage}>
                    <Text
                      style={{
                        textAlign: "center",
                        alignSelf: "center",
                        alignItems: "center",
                        justifyContent: "center",
                        borderRadius: 15,
                        position: "absolute",
                        bottom: 0,
                        height: 20,
                        width: "100%",
                        color: "white",
                        backgroundColor: "black",
                        overflow: "hidden",
                        fontSize: 12,
                      }}
                    >
                      Upload
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.userDetailView}>
                  {/* <Text style={styles.userName}>{user.name}</Text> */}
                  <Text style={styles.userName}>{`${userProfileData.first_name} ${userProfileData.last_name}`}</Text>
                  <Text style={styles.userProf}>View Profile</Text>
                </View>
                <View style={{ width: "15%" }}></View>
                <TouchableOpacity onPress={this._changeIcon}>
                  <Image
                    source={show === false ? DownArrow : UpArrow}
                    style={styles.accordianLogo}
                  />
                </TouchableOpacity>
              </View>
              {show && (
                <View style={styles.fnameinputs}>
                  <CommonInput
                    value={firstName}
                    defaultValue={firstName}
                    lockVisible={true}
                    userVisible={false}
                    inputStyle={styles.emailinput2}
                    placeholder={firstName}
                    placeholderTextColor={Colors.Black_Color}
                    onChangeText={this.inputChangeHandler("firstName")}
                  />
                </View>
              )}
              {show && (
                <View style={styles.nameinputs}>
                  <CommonInput
                    value={lastName}
                    defaultValue={lastName}
                    lockVisible={true}
                    userVisible={false}
                    inputStyle={styles.emailinput2}
                    placeholder={lastName}
                    placeholderTextColor={Colors.Black_Color}
                    onChangeText={this.inputChangeHandler("lastName")}
                  />
                </View>
              )}
              {show && (
                <View style={styles.nameinputs}>
                  <CommonInput
                    value={email}
                    // defaultValue={email}
                    lockVisible={true}
                    userVisible={false}
                    inputStyle={styles.emailinput2}
                    placeholder={email}
                    placeholderTextColor={Colors.Black_Color}
                    onChangeText={this.inputChangeHandler("email")}
                  />
                </View>
              )}
              {show && (
                <View style={styles.nameinputs}>
                  <CommonInput
                    value={PhoneNumber}
                    lockVisible={true}
                    userVisible={false}
                    inputStyle={styles.emailinput2}
                    placeholder={PhoneNumber}
                    placeholderTextColor={Colors.Black_Color}
                    onChangeText={this.inputChangeHandler("PhoneNumber")}
                  />
                </View>
              )}
              {/* {show && (
                <View style={styles.profileView}>
                  <View style={styles.profileImgView}>
                    <View style={styles.profileUriImg}>
                      {imageSource === null ? (
                        <Image
                          source={UserPofileImage}
                          style={styles.profileImg}
                        />
                      ) : (
                        <Image
                          source={{ uri: imageSource }}
                          style={styles.profileImg2}
                        />
                      )}
                    </View>
                    <View style={{ width: "10%" }}></View>
                    <View style={styles.uploadBtnView}>
                      <TouchableOpacity
                        style={styles.uploadBtn}
                        onPress={this.UploadImage}
                      >
                        <Text style={styles.logoutBtnText}>Upload</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )} */}
              {show && (
                // <View style={[styles.nameinputs, { marginTop: "5%" }]}>
                <View style={styles.nameinputs}>
                  <CommonInput
                    value={bedgeId}
                    defaultValue={bedgeId}
                    lockVisible={true}
                    userVisible={false}
                    inputStyle={styles.emailinput2}
                    placeholder={bedgeId}
                    placeholderTextColor={Colors.Black_Color}
                    onChangeText={this.inputChangeHandler("bedgeId")}
                  />
                </View>
              )}
              {show && (
                <View style={[styles.nameinputs, { marginTop: "5%" }]}>
                  <MultiSelect
                    hideTags
                    hideSubmitButton
                    items={roles}
                    uniqueKey="id"
                    ref={(component) => {
                      this.multiSelect = component;
                    }}
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    selectedItems={selectedItems}
                    selectText="Roles"
                    fontSize={18}
                    itemFontSize={18}
                    itemTextColor={Colors.Black_Color}
                    itemFontFamily={Fonts.fontRegular}
                    selectedItemFontFamily={Fonts.fontRegular}
                    textColor={Colors.Black_Color}
                    styleItemsContainer={{
                      width: "100%",
                      alignSelf: "center",
                    }}
                    styleDropdownMenu={{
                      width: "100%",
                      // borderBottomWidth: 1,
                      // borderBottomColor: Colors.BorderColor,
                    }}
                    styleTextDropdown={{ fontFamily: Fonts.fontSemiBold }}
                    styleTextDropdownSelected={{
                      width: "100%",
                      height: "auto",
                      fontFamily: Fonts.fontRegular,
                    }}
                    styleInputGroup={{ width: "100%", alignSelf: "center" }}
                    styleMainWrapper={{ paddingLeft: 20 }}
                    // fontFamily={Fonts.fontMedium}
                    // altFontFamily={Fonts.fontMedium}
                    // styleRowList={{
                    //   borderBottomWidth: 1,
                    //   width: "100%",
                    //   alignSelf: "center",
                    //   borderColor: Colors.BorderColor,
                    // }}
                    // styleSelectorContainer= {{borderBottomWidth: 1}}
                    // selectedItemTextColor={Colors.Black_Color}
                    //  styleDropdownMenuSubsection={{
                    //   paddingLeft: 20,
                    //   width: '85%'
                    // }}
                  />
                </View>
              )}
              {show && (
                <View style={styles.accordianBtn}>
                  {/* <CommonButton
                    btnName="Cancle"
                    style={[styles.cancleBtn, styles.shadow]}
                    textStyle={styles.logoutBtnText}
                  /> */}
                  <CommonButton
                    btnName="Save"
                    style={[styles.saveBtn, styles.shadow]}
                    textStyle={styles.logoutBtnText}
                    onPress={this.saveAccountDetail}
                  />
                </View>
              )}
              <View>
                <CommonButton
                  btnName="Logout"
                  style={[styles.logoutBtn, styles.shadow]}
                  textStyle={styles.logoutBtnText}
                  onPress={this.signOut}
                />
                <View style={styles.bottomView}>
                  <Image source={PoweredByLogo} style={styles.bottomContent} />
                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default Settings;
