import { StyleSheet, Dimensions } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.0,
  },
  footerView: {
    flex: 5.0,
    height: height,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  logoutBtn: {
    width: "85%",
    marginTop: "5%",
    borderRadius: 50,
    alignSelf: "center",
    backgroundColor: Colors.Join_Btn,
  },
  logoutBtnText: {
    textAlign: "center",
    fontSize: FontSize.font13,
    color: Colors.white_Color,
    fontFamily: Fonts.fontMedium,
  },
  bottomView: {
    alignItems: "center",
    marginTop: "10%",
    paddingBottom: 10,
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  textview: {
    width: "85%",
    marginTop: "10%",
    alignItems: "center",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  userimageview: {
    justifyContent: "center",
    alignItems: "center",
  },
  userDetailView: {
    flexDirection: "column",
    justifyContent: "space-around",
  },
  userProf: {
    fontSize: FontSize.font13,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  userName: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  profileView: {
    width: "85%",
    alignSelf: "center",
    marginTop: "8%",
  },
  profileImgView: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
  profileUriImg: {
    backgroundColor: Colors.Light_Text,
    borderRadius: 40,
    borderColor: Colors.BorderColor,
    borderWidth: 1,
    height: "auto",
    overflow: "hidden",
  },
  profileImg: {
    width: RFontSize(68),
    height: RFontSize(68),
    resizeMode: "contain",
  },
  profileImg2: {
    width: RFontSize(68),
    height: RFontSize(68),
    resizeMode: "cover",
  },
  uploadBtnView: {
    width: "70%",
    flexWrap: "wrap",
  },
  uploadBtn: {
    width: "45%",
    borderRadius: 50,
    padding: 6,
    alignSelf: "center",
    backgroundColor: Colors.Primary_Color,
  },
  accordianLogo: {
    width: RFontSize(25),
    height: RFontSize(25),
    resizeMode: "contain",
  },
  fnameinputs: {
    width: "85%",
    marginTop: "8%",
    alignSelf: "center",
  },
  nameinputs: {
    width: "85%",
    marginTop: "2%",
    alignSelf: "center",
  },
  emailinput2: {
    borderColor: Colors.BorderColor,
    textAlign: "center",
  },
  accordianBtn: {
    width: "100%",
    marginTop: '10%',
    alignSelf: "center",
  },
  cancleBtn: {
    width: "45%",
    backgroundColor: "red",
    borderRadius: 50,
    backgroundColor: Colors.Join_Btn,
  },
  saveBtn: {
    width: "85%",
    backgroundColor: "blue",
    borderRadius: 50,
    alignSelf: "center",
    backgroundColor: Colors.Primary_Color,
  },

  tagArea: {
    flexDirection: "row",
    width: "100%",
    marginBottom: 5,
    paddingVertical: 3,
    alignItems: "center",
  },
  tagNameView: {
    width: "30%",
    flexWrap: "wrap",
    alignItems: "center",
  },
  tagName: {
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontRegular,
    color: Colors.OrderCodeColor,
  },
  tagValueView: {
    width: "60%",
    flexWrap: "wrap",
    alignItems: "center",
  },
  tagValue: {
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontSemiBold,
    color: Colors.OrderCodeColor,
  },
  middleView: {
    width: "10%",
    alignItems: "center",
  },
});
