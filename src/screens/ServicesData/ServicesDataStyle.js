import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../theme/index";
import { FontSize, Fonts } from "../../constant/index";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.0,
  },
  footerView: {
    flex: 5.0,
    height: height,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
    paddingVertical: "10%",
  },
  flexmainview: {
    height: "auto",
    width: "90%",
    backgroundColor: Colors.white_Color,
    marginTop: "3%",
    marginBottom: "3%",
    padding: 20,
    alignSelf: "center",
    borderRadius: 20,
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  nodataView: {
    alignSelf: "center",
    marginTop: "40%",
  },
  noDataText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  tagArea: {
    flexDirection: "row",
    width: "100%",
    marginBottom: 5,
    paddingVertical: 3,
    alignItems: "center",
  },
  tagNameView: {
    width: "30%",
    flexWrap: "wrap",
    alignItems: "center",
  },
  tagName: {
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontRegular,
    color: Colors.OrderCodeColor,
  },
  tagValueView: {
    width: "60%",
    flexWrap: "wrap",
    alignItems: "center",
  },
  tagValue: {
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontSemiBold,
    color: Colors.OrderCodeColor,
  },
  middleView: {
    width: "10%",
    alignItems: "center",
  },
});
