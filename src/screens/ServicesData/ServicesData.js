import React, { Component } from "react";
import { FlatList, Text, View, ScrollView } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Commonbackground } from "../../components";
import styles from "./ServicesDataStyle";
import { CartIcon, Kaktus_Logo } from "../../assets";

export class ServicesData extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { data } = this.props.route.params;
    let orders = data.results;
    return (
      <>
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            backPress={this.goToBackScreen}
          />
          <View style={styles.footerView}>
            {orders && orders.length > 0 ? (
              <FlatList
                data={data.results}
                isLoading={true}
                renderItem={({ item, index }) => {
                  return (
                    <View style={[styles.flexmainview, styles.shadow]}>
                      <View style={styles.tagArea}>
                        <View style={styles.tagNameView}>
                          <Text style={styles.tagName} numberOfLines={1}>
                            FullName
                          </Text>
                        </View>
                        <View style={styles.middleView}>
                          <Text> : </Text>
                        </View>
                        <View style={styles.tagValueView}>
                          <Text
                            style={styles.tagValue}
                            numberOfLines={1}
                          >{`${item.first_name} ${item.last_name}`}</Text>
                        </View>
                      </View>
                      <View style={styles.tagArea}>
                        <View style={styles.tagNameView}>
                          <Text style={styles.tagName} numberOfLines={1}>
                            PU Code
                          </Text>
                        </View>
                        <View style={styles.middleView}>
                          <Text> : </Text>
                        </View>
                        <View style={styles.tagValueView}>
                          <Text style={styles.tagValue} numberOfLines={1}>
                            {item.pucode}
                          </Text>
                        </View>
                      </View>
                      {item.name ? (
                        <View style={styles.tagArea}>
                          <View style={styles.tagNameView}>
                            <Text style={styles.tagName} numberOfLines={1}>
                              Box-Name
                            </Text>
                          </View>
                          <View style={styles.middleView}>
                            <Text> : </Text>
                          </View>
                          <View style={styles.tagValueView}>
                            <Text style={styles.tagValue} numberOfLines={1}>
                              {item.name}
                            </Text>
                          </View>
                        </View>
                      ) : (
                        <View style={styles.tagArea}>
                          <View style={styles.tagNameView}>
                            <Text style={styles.tagName} numberOfLines={2}>
                              Storage Location
                            </Text>
                          </View>
                          <View style={styles.middleView}>
                            <Text> : </Text>
                          </View>
                          <View style={styles.tagValueView}>
                            <Text style={styles.tagValue} numberOfLines={1}>
                              {item.storage_location}
                            </Text>
                          </View>
                        </View>
                      )}

                      <View style={styles.tagArea}>
                        <View style={styles.tagNameView}>
                          <Text style={styles.tagName} numberOfLines={1}>
                            Stock Code
                          </Text>
                        </View>
                        <View style={styles.middleView}>
                          <Text> : </Text>
                        </View>
                        <View style={styles.tagValueView}>
                          <Text style={styles.tagValue} numberOfLines={1}>
                            {item.stock_code}
                          </Text>
                        </View>
                      </View>

                      {/* <View style={styles.tagArea}>
                        <View style={styles.tagNameView}>
                          <Text style={styles.tagName} numberOfLines={1}>
                            Status
                          </Text>
                        </View>
                        <View style={styles.middleView}>
                          <Text> : </Text>
                        </View>
                        <View style={styles.tagValueView}>
                          <Text style={styles.tagValue} numberOfLines={1}>
                            {item.status}
                          </Text>
                        </View>
                      </View> */}
                    </View>
                  );
                }}
              />
            ) : (
              <View style={styles.nodataView}>
                <Text style={styles.noDataText}>No Data found!</Text>
              </View>
            )}
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default ServicesData;
