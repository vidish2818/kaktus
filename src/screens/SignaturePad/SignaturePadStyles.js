import { StyleSheet, Dimensions } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1,
  },
  footerView: {
    flex: 5.2,
    height: height,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  headerView: {
    width: "90%",
    marginTop: "4%",
    alignSelf: "center",
    marginBottom: "8%",
  },
  headerText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
});
