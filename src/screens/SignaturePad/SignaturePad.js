import { I18n } from "../../constant";
import { CartIcon, Kaktus_Logo } from "../../assets";
import React, { Component, useRef } from "react";
import styles from "./SignaturePadStyles";
import { Commonbackground } from "../../components";
import Signature from "react-native-signature-canvas";
import {
  Text,
  View,
  SafeAreaView,
  Dimensions,
  ScrollView,
  StyleSheet,
} from "react-native";
import enums from "../../enums/enums";
import { BASE_API_URL_UPDATE } from "../../config/config";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { removeItem } from "../../redux/Actions/addToCart.action";
import Orientation from "react-native-orientation";

const { height, width } = Dimensions.get("window");

export class SignaturePad extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headingName: "Signature",
      drawSignature: null,
      userToken: "",
      receiptData: this.props.route.params,
    };
  }

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userToken: data });
  }

  handleSignature = (signature) => {
    // const { receiptData } = this.state;

    // this.props.removeDataToCart(receiptData);

    const { userToken } = this.state;
    let token = userToken?.signInUserSession?.idToken?.jwtToken;

    const { data } = this.props.route.params;
    const customerObj = data;
    // data && data.barcodeScannedData && data.barcodeScannedData.response;

    let orderIds = [];
    orderIds.push(data.OrderId);

    let _orderIds = [];
    _orderIds.push({ id: data.OrderId });

    let updateStatus = {
      event_name: "INSERT",
      event_type_name: "COMPLETED",
      description: `Kaktus Pharmacy: Prescription status is updated to COMPLETED for pickup code ${customerObj.pucode}`,
      object_id: enums.Completed,
      tofor_id: customerObj.OrderId,
      box_id: customerObj.BoxId,
      customer_id: customerObj.PatientId,
      type_id: enums.Completed,
      order_id: customerObj.OrderId,
      account_id: customerObj.patient.ParentAccountId,
      location_notes: "",
    };

    let saveKioskCustomer = {
      customer_id: customerObj.PatientId,
      orderIds: JSON.stringify(_orderIds),
      order_id: customerObj.OrderId,
      picture_url: null,
      signature_url: signature,
      drivers_license_no: null,
      first_name: customerObj.patient.FirstName,
      middle_name: "",
      last_name: customerObj.patient.LastName,
      name: customerObj.patient.PatientName,
      dob: customerObj.patient.Dob,
      relationship: "Patient",
      box_id: customerObj.BoxId,
      account_id: customerObj.patient.ParentAccountId,
      selectedstatecode: null,
      dlrawdata: null,
      is_consult_requested: false,
      kioskBoxAccountConnection: "",
    };

    fetch(`${BASE_API_URL_UPDATE}/saveKioskCustomerCollectedDetail`, {
      method: "POST",
      withCredentials: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(saveKioskCustomer),
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {});

    fetch(`${BASE_API_URL_UPDATE}/deliveryconsole/updatestatus`, {
      method: "POST",
      withCredentials: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updateStatus),
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        this.props.navigation.goBack();
      });
  };

  handleEmpty = () => {
    console.log("Empty");
  };

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { headingName, receiptData } = this.state;
    const { data } = this.props.route.params;

    const styleee = `.m-signature-pad--footer
    .button {
      background-color: '#0099DC';
      color: #FFF;
      width: 120px;
      font-size: 15px;
      font-Weight: bold;
      }
      .m-signature-pad {
      font-size: 10px;
      width: 100%;
      height: 350px;
      align-self: center;
      border: 1px solid #e8e8e8;
      border-radius: 10px;
      },
      .m-signature-pad--body {
      position: absolute;
      left: 60px;
      right: 20px;
      top: 20px;
      bottom: 60px;
      border: 1px solid #f4f4f4;
      }
    `;

    return (
      <>
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            style={styles.headerSection}
            mainLogo={Kaktus_Logo}
            backPress={this.goToBackScreen}
          />
          <View style={styles.footerView}>
            {/* <ScrollView
            showsVerticalScrollIndicator={false}
            // contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
            style={{ flex: 1, paddingBottom: 400 }}
            > */}
            <View style={styles.headerView}>
              <Text style={styles.headerText}>{I18n.t("signatureScreen")}</Text>
            </View>
            <View
              style={{
                flex: 1,
                width: "100%",
                height: "auto",
              }}
            >
              <Signature
                onOK={this.handleSignature}
                onEmpty={this.handleEmpty}
                descriptionText=""
                clearText="Clear"
                confirmText="Submit"
                autoClear={false}
                webStyle={styleee}
              />
            </View>
            {/* {/ </ScrollView> /} */}
          </View>
        </SafeAreaView>
      </>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     AllOrders: state.OrderDetails,
//     cartsData: state.CartData,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     removeDataToCart: (data) => {
//       dispatch(removeItem(data));
//     },
//   };
// };
// export default connect(mapStateToProps)(SignaturePad);

export default SignaturePad;
