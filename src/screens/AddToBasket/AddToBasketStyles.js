import {StyleSheet, Dimensions} from 'react-native';
import {Fonts, FontSize} from '../../constant';
import {Colors} from '../../theme';
import {RFontSize} from '../../utils';

const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 0.8,
  },
  footerView: {
    flex: 5.2,
    height: height,
    paddingTop: 3,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  headerView: {
    width: '90%',
    marginTop: '8%',
    alignSelf: 'center',
  },
  headerView2: {
      width: "90%",
      alignSelf: 'center'
  },
  headerText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  addressSection: {
    width: '90%',
    alignSelf: 'center',
    marginTop: '5%',
  },
  shadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  deliverBtn: {
    width: '45%',
    marginTop: '10%',
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: Colors.Join_Btn,
  },
  btnText: {
    textAlign: 'center',
    fontSize: FontSize.font13,
    color: Colors.white_Color,
    fontFamily: Fonts.fontMedium,
  },
  bottomView: {
    marginTop: '10%',
    alignSelf: 'center',
    paddingBottom: 10
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: 'contain',
  }
});
