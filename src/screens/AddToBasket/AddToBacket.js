import React, { Component } from "react";
import {
  SafeAreaView,
  ScrollView,
  ScrollViewBase,
  View,
  Text,
} from "react-native";
import styles from "./AddToBasketStyles";
import {
  AllOrdersListBox,
  CommonAddressBox,
  Commonbackground,
  CommonButton,
} from "../../components";
import {
  Kaktus_Logo,
  CartIcon,
  ForwardArrow,
  DownArrow,
  UpArrow,
  PoweredByLogo,
} from "../../assets";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";

export class AddToBacket extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Commonbackground
          cartBtn={CartIcon}
          addtocart={this.addToCart}
          style={styles.headerSection}
          backPress={this.goToBackScreen}
        />
        <View style={styles.footerView}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
            style={{ flex: 1, paddingBottom: 400 }}
          >
            <View style={styles.headerView}>
              <Text style={styles.headerText}>Orders Ready to Delivers</Text>
            </View>
            <View style={styles.headerView}>
              <Text
                style={{
                  fontSize: FontSize.font16,
                  color: Colors.Primary_Color,
                  fontFamily: Fonts.fontSemiBold,
                }}
              >
                Pending Orders
              </Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default AddToBacket;
