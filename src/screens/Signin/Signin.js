import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from "react-native";
import { Commonbackground, CommonInput } from "../../components";
import styles from "./SigninStyles";
import {
  Kaktus_Logo,
  UserLogo,
  LockIcon,
  EyeOn,
  Facebook,
  Google,
  Okta,
  EyeOff,
  PoweredByLogo,
} from "../../assets";
import { Colors } from "../../theme";
import { I18n } from "../../constant";
import { CommonButton, CommonLoader } from "../../components";
import { Auth } from "aws-amplify";
import AsyncStorage from "@react-native-community/async-storage";
import Toast from "react-native-simple-toast";
import { BASE_API_URL, BASE_API_URL_Services } from "../../config/config";

class Signin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      errors: {},
      show: false,
      visible: true,
      isLoading: false,
      itemCartLength: 0,
      userToken: "",
    };
  }
  inputChangeHandler = (name) => (event) => {
    this.setState({
      [name]: event,
    });
  };

  _changeIcon = () => {
    const { show, visible } = this.state;
    this.setState({
      show: !show,
      visible: !visible,
      cartRecords: [],
      // isLoading: false,
    });
  };

  signIn = async () => {
    this.setState({
      instruction: "",
      isLoading: false,
    });
    try {
      const { email, password } = this.state;
      const user = await Auth.signIn(email, password);
      if (user?.signInUserSession?.idToken?.jwtToken) {
        fetch(`${BASE_API_URL}/order-by-code/get-cart`, {
          method: "GET",
          withCredentials: true,
          headers: {
            Authorization:
              "Bearer " + user?.signInUserSession?.idToken?.jwtToken,
            "Content-Type": "application/json",
          },
        })
          .then((response) => {
            return response.json();
          })
          .then((res) => {
          });
      }
      AsyncStorage.setItem("user", JSON.stringify(user));
      this.props.navigation.replace("AppStack");
      this.setState({
        isLoading: true,
      });
    } catch (error) {
      if (error && error.message) {
        Toast.show(error.message, Toast.LONG);
      }
    }
  };

  render() {
    const { email, password, errors, isLoading, show, visible } = this.state;
    return (
      <>
        <CommonLoader loading={isLoading} />
        <SafeAreaView style={styles.container}>
          <Commonbackground
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            logoStyle={styles.topLogo}
          />
          <View style={styles.footerView}>
            <View style={styles.mainHeader}>
              <Text style={styles.mainHeading}>{I18n.t("signInWelcome")}</Text>
            </View>
            <View style={styles.subHeader}>
              <Text style={styles.subHeading}>{I18n.t("signInContinue")}</Text>
            </View>
            <View style={styles.emailinputs}>
              <CommonInput
                value={email}
                userVisible={true}
                lockVisible={true}
                userIcon={UserLogo}
                placeholder={I18n.t("emailText")}
                autoCapitalize="none"
                placeholderTextColor={Colors.Black_Color}
                // onChangeText={this.handleEmail}
                onChangeText={this.inputChangeHandler("email")}
              />
            </View>
            <View style={styles.pswinputs}>
              <CommonInput
                value={password}
                userVisible={true}
                lockVisible={true}
                userIcon={LockIcon}
                eyeIcon={show === false ? EyeOn : EyeOff}
                placeholder={I18n.t("pswText")}
                secureTextEntry={visible}
                autoCapitalize="none"
                eyeOnpress={this._changeIcon}
                placeholderTextColor={Colors.Black_Color}
                // onChangeText={this.handlePassword}
                onChangeText={this.inputChangeHandler("password")}
              />
            </View>
            <TouchableOpacity style={styles.forgotView}>
              <Text style={styles.forgotText}>{I18n.t("forgotText")}</Text>
            </TouchableOpacity>
            <CommonButton
              btnName={I18n.t("loginText")}
              style={[styles.loginBtn, styles.shadow]}
              textStyle={styles.loginText}
              onPress={this.signIn}
            />
            <View style={{ alignItems: "center", paddingVertical: "10%" }}>
              <Text style={styles.errormsg}>{this.state.instruction}</Text>
            </View>
            {/* <View style={styles.loginOptions}>
              <Text style={styles.optionText}>{I18n.t('socialText')}</Text>
            </View>
            <View style={styles.socialBtns}>
              <TouchableOpacity>
                <Image source={Facebook} style={styles.socialLogos} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={Google} style={styles.socialLogos} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={Okta} style={styles.socialLogos} />
              </TouchableOpacity>
            </View> */}
            <View style={styles.bottomView}>
              <Image source={PoweredByLogo} style={styles.bottomContent} />
            </View>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default Signin;
