import { StyleSheet, Dimensions } from "react-native";
import { Colors } from "../../theme";
import { Fonts, FontSize } from "../../constant";
import { RFontSize } from "../../utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.3,
  },
  footerView: {
    flex: 4.7,
    height: height,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  mainHeader: {
    width: "90%",
    height: "auto",
    marginTop: "5%",
    alignSelf: "center",
  },
  mainHeading: {
    fontSize: FontSize.font30,
    color: Colors.Primary_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  subHeader: {
    width: "90%",
    height: "auto",
    alignSelf: "center",
  },
  subHeading: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  emailinputs: {
    width: "80%",
    marginTop: "8%",
    alignSelf: "center",
  },
  pswinputs: {
    width: "80%",
    marginTop: "6%",
    alignSelf: "center",
  },
  forgotView: {
    width: "80%",
    paddingTop: 10,
    alignSelf: "center",
    alignItems: "flex-end",
  },
  forgotText: {
    fontSize: FontSize.font14,
    color: Colors.Primary_Color,
    fontFamily: Fonts.fontRegular,
  },
  loginBtn: {
    width: wp("80%"),
    marginTop: "8%",
    borderRadius: 50,
    alignSelf: "center",
    backgroundColor: Colors.Primary_Color,
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  loginText: {
    fontSize: FontSize.font16,
    textAlign: "center",
    // color: Colors.white_Color,
    // fontFamily: Fonts.fontSemiBold,
    // fontSize: RFontSize(16),
    color: Colors.white_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  errormsg: {
    color: Colors.ErrorColor,
    fontFamily: Fonts.fontMedium,
  },
  loginOptions: {
    marginTop: "8%",
    alignSelf: "center",
  },
  optionText: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  socialBtns: {
    width: wp("50%"),
    marginTop: "8%",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  socialLogos: {
    width: RFontSize(40),
    height: RFontSize(40),
    resizeMode: "contain",
  },
  bottomView: {
    bottom: 10,
    alignSelf: "center",
    position: "absolute",
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  topLogo: {
    marginTop: "10%",
  },
});
