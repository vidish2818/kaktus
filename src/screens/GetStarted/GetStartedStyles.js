import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";
import { Fonts, FontSize } from "../../constant";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerView: {
    flex: 3.5,
  },
  footerView: {
    flex: 2.5,
  },
  mainText: {
    paddingBottom: 2,
    fontSize: FontSize.font24,
    color: Colors.Primary_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  subText: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  loginBtn: {
    width: "80%",
    marginTop: "8%",
    borderRadius: 50,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.Primary_Color,
  },
  loginText: {
    // fontSize: RFontSize(16),
    fontSize: FontSize.font16,
    color: Colors.white_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  bottomView: {
    bottom: 5,
    position: "absolute",
    alignSelf: "center",
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  roundLogo: {
    top: 0,
    right: -50,
    position: "absolute",
    width: RFontSize(326),
    height: RFontSize(326),
  },
  mainLogo: {
    top: "10%",
    alignSelf: "center",
    position: "absolute",
    width: RFontSize(178),
    height: RFontSize(59),
    resizeMode: "contain",
  },
  graphicsImg: {
    top: "40%",
    width: "90%",
    alignSelf: "center",
    position: "absolute",
    resizeMode: "contain",
    height: RFontSize(230),
  },
});
