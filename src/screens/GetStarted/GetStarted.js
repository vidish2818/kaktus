import React, { Component } from "react";
import styles from "./GetStartedStyles";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ImageBackground,
  ScrollView,
  Dimensions,
} from "react-native";
import {
  PoweredByLogo,
  Bg_Img,
  Kaktus_Logo,
  Graphics,
  Round_Bg,
} from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as roc,
} from "react-native-responsive-screen";
import * as Animatable from "react-native-animatable";
import { CommonButton } from "../../components";
import { I18n } from "../../constant";

const { height, width } = Dimensions.get("window");

export class GetStarted extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    loc(this);
  }

  componentWillUnMount() {
    rol();
  }

  navigateScreen = () => {
    this.props.navigation.navigate("Signin");
  };

  render() {
    return (
      <>
        <SafeAreaView style={styles.container}>
          <ImageBackground
            source={Bg_Img}
            style={{ width: wp("100%"), height: hp("100%") }}
          >
            <View style={[styles.headerView]}>
              <Image source={Round_Bg} style={styles.roundLogo} />
              <Animatable.Image
                duration={2000}
                animation="fadeInUpBig"
                source={Kaktus_Logo}
                style={styles.mainLogo}
              />
              <Animatable.Image
                duration={2000}
                animation="bounceIn"
                source={Graphics}
                style={styles.graphicsImg}
              />
            </View>
            <View style={styles.footerView}>
              <Animatable.View
                duration={2000}
                animation="flipInY"
                style={{ marginTop: "15%", alignSelf: "center" }}
              >
                <Text style={styles.mainText}>{I18n.t("welcomeText")}</Text>
              </Animatable.View>
              <Animatable.View
                animation="tada"
                duration={3000}
                style={{ alignSelf: "center" }}
              >
                <Text style={styles.subText}>{I18n.t("welcomeSubText")}</Text>
              </Animatable.View>
              <CommonButton
                style={styles.loginBtn}
                btnName={I18n.t("loginText")}
                textStyle={styles.loginText}
                onPress={this.navigateScreen}
              />
            </View>
          </ImageBackground>
          <View style={styles.bottomView}>
            <Image source={PoweredByLogo} style={styles.bottomContent} />
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default GetStarted;
