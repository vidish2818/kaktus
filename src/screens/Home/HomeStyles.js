import { StyleSheet, Dimensions } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.0,
  },
  footerView: {
    flex: 5.0,
    height: height,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  textview: {
    width: "85%",
    marginTop: 20,
    alignItems: "center",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  userimageview: {
    justifyContent: "center",
    alignItems: "center",
  },
  userDetailView: {
    flexDirection: "column",
    justifyContent: "space-around",
  },
  userProf: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  userName: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  profileImg: {
    width: RFontSize(68),
    height: RFontSize(68),
    resizeMode: "contain",
  },
  profileImg2: {
    width: RFontSize(68),
    height: RFontSize(68),
    resizeMode: "cover",
    borderRadius: 40
  },
  scannerview: {
    width: "80%",
    height: RFontSize(150),
    marginTop: "8%",
    borderRadius: 20,
    marginBottom: "5%",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  scannerIcon: {
    width: RFontSize(35),
    height: RFontSize(35),
    resizeMode: "contain",
    alignSelf: "center",
  },
  scannerText: {
    fontSize: FontSize.font13,
    marginTop: 15,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  scannerimg: {
    height: RFontSize(150),
    width: "47%",
    borderRadius: 20,
    marginBottom: "6%",
    alignItems: "center",
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  flatlistWrapper: {
    width: "80%",
    alignSelf: "center",
    justifyContent: "space-between",
  },
  serviceText: {
    fontSize: FontSize.font13,
    marginTop: 10,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontMedium,
  },
  otherServices: {
    height: hp("8%"),
    width: wp("80%"),
    paddingHorizontal: 15,
    marginTop: "5%",
    borderRadius: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: Colors.white_Color,
  },
  firstSection: {
    justifyContent: "center",
  },
  mainName: {
    fontSize: FontSize.font16,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  subName: {
    fontSize: FontSize.font12,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontRegular,
  },
  secondSection: { justifyContent: "center" },
  bottomView: {
    marginTop: "15%",
    alignSelf: "center",
    paddingBottom: 10,
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  userimgview: {
    // backgroundColor: Colors.Light_Text,
    borderRadius: 40,
    borderColor: Colors.BorderColor,
    borderWidth: 1,
    height: "auto",
    overflow: "hidden",
  },
});
