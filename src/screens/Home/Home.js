import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import {
  Scanner,
  CartIcon,
  ChatService,
  Kaktus_Logo,
  ForwardArrow,
  PoweredByLogo,
  ReturnService,
  DeliverService,
  UserPofileImage,
  VirtualVisitService,
} from "../../assets";
import styles from "./HomeStyles";
import { I18n } from "../../constant";
import {
  Commonbackground,
  CommonDeliverOrder,
  CommonLoader,
} from "../../components";
import {
  BASE_API_URL,
  BASE_API_URL_Services,
  BASE_API_URL_UPDATE,
} from "../../config/config";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { getCartRecords } from "../../redux/Actions/getCartRecords";

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allServices: [
        { serviceIcon: DeliverService, serviceName: I18n.t("service1") },
        { serviceIcon: ReturnService, serviceName: I18n.t("service2") },
        { serviceIcon: VirtualVisitService, serviceName: I18n.t("service3") },
        { serviceIcon: ChatService, serviceName: I18n.t("service4") },
      ],
      userName: "ABC",
      isLoading: false,
      kioskData: [],
      userData: "",
      imageSource: null,
      userProfileData: "",
    };
  }

  scanQrCode = () => {
    this.props.navigation.navigate("Qrcode");
  };

  // componentDidMount() {
  //   this.interval = setInterval(() => {
  //     this.getKioskService();
  //     this.getCurbSideService();
  //     this.getCounterService();
  //     this.getCourierService();
  //     this.getBedSideService();
  //   });
  // }
  // componentWillUnmount() {
  //   clearInterval(this.interval);
  // }

  // getKioskClick = () => {
  //   const { kioskData } = this.state;
  //   this.props.navigation.navigate("ServicesData", {
  //     data: kioskData,
  //   });
  // };

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userData: data });
    this.cartRecordsData();
    this.getAccountDetail();
  }

  cartRecordsData = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    this.props.cartRecords(token);
  };

  getKioskService = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    console.log("kiosktoken", token);
    try {
      this.setState({
        isLoading: true,
      });
      fetch(`${BASE_API_URL_Services}/kiosk`, {
        method: "POST",
        withCredentials: true,
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pageNo: 1,
          pageSize: 10,
          scanText: "",
          searchText: "",
          sortColumn: "created_on",
          sortOrder: "desc",
        }),
      })
        .then((response) => response.json())
        .then((res) => {
          console.log("kiosk", res);
          this.props.navigation.navigate("ServicesData", {
            data: res,
          });
          this.setState({
            isLoading: false,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  getCurbSideService = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    try {
      this.setState({
        isLoading: true,
      });
      fetch(`${BASE_API_URL_Services}/curbside`, {
        method: "POST",
        withCredentials: true,
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pageNo: 1,
          pageSize: 10,
          scanText: "",
          searchText: "",
          sortColumn: "modified_on",
          sortOrder: "desc",
        }),
      })
        .then((response) => response.json())
        .then((res) => {
          this.props.navigation.navigate("ServicesData", {
            data: res,
          });
          this.setState({
            isLoading: false,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  getCounterService = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    try {
      this.setState({
        isLoading: true,
      });
      fetch(`${BASE_API_URL_Services}/counter`, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pageNo: 1,
          pageSize: 10,
          scanText: "",
          searchText: "",
          sortColumn: "created_on",
          sortOrder: "desc",
        }),
      })
        .then((response) => response.json())
        .then((res) => {
          this.props.navigation.navigate("ServicesData", {
            data: res,
          });
          this.setState({
            isLoading: false,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  getCourierService = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    try {
      this.setState({
        isLoading: true,
      });
      fetch(`${BASE_API_URL_Services}/courier`, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pageNo: 1,
          pageSize: 10,
          scanText: "",
          searchText: "",
          sortColumn: "created_on",
          sortOrder: "desc",
        }),
      })
        .then((response) => response.json())
        .then((res) => {
          this.props.navigation.navigate("ServicesData", {
            data: res,
          });
          this.setState({
            isLoading: false,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  getBedSideService = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    try {
      this.setState({
        isLoading: true,
      });
      fetch(`${BASE_API_URL_Services}/bedside`, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pageNo: 1,
          pageSize: 10,
          scanText: "",
          searchText: "",
          sortColumn: "created_on",
          sortOrder: "desc",
        }),
      })
        .then((response) => response.json())
        .then((res) => {
          this.props.navigation.navigate("ServicesData", {
            data: res,
          });
          this.setState({
            isLoading: false,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  UploadImage = () => {
    launchImageLibrary(this.option, (response) => {
      console.log({ response });
      if (response.didCancel) {
        console.log("User cancelled photo picker");
        Toast.show("You did not select any image", Toast.LONG);
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        let source = { uri: response.uri };
        console.log({ source });

        this.setState({
          imageSource: source.uri,
        });
      }
    });
  };
  getAccountDetail = () => {
    const { userData } = this.state;
    let token = userData?.signInUserSession?.idToken?.jwtToken;
    console.log("tokennnnn", token);

    fetch(`${BASE_API_URL_UPDATE}/users/${userData?.username}`, {
      method: "GET",
      withCredentials: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        this.setState({
          imageSource: res[0].picture_url,
          userProfileData: res[0],
        });
      });
  };

  navigateParticularScreen = ({ item, index }) => {
    if (index === 0) {
      this.props.navigation.navigate("Deliver");
    } else if (index === 1) {
      console.log("Index 1");
    } else if (index === 2) {
      console.log("Index 2");
    } else if (index === 3) {
      console.log("Index 3");
    }
  };

  render() {
    const {
      allServices,
      isLoading,
      userData,
      imageSource,
      userProfileData,
    } = this.state;
    let user = userData && userData.attributes && userData.attributes;

    return (
      <>
        <CommonLoader loading={isLoading} />
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={false}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
          />

          <View style={styles.footerView}>
            <FlatList
              ListHeaderComponent={
                <>
                  <View style={styles.textview}>
                    <View style={styles.userDetailView}>
                      <Text style={styles.userProf}>{I18n.t("workAt")}</Text>
                      <Text
                        style={styles.userName}
                      >{`${userProfileData.first_name} ${userProfileData.last_name}`}</Text>
                    </View>
                    <View style={styles.userimgview}>
                    {imageSource === null ? (
                        <Image
                          source={UserPofileImage}
                          style={styles.profileImg}
                        />
                      ) : (
                        <Image
                          source={{ uri: imageSource }}
                          style={styles.profileImg2}
                        />
                      )}
                    </View>
                  </View>
                  <TouchableOpacity
                    style={[styles.scannerview, styles.shadow]}
                    onPress={this.scanQrCode}
                  >
                    <Image source={Scanner} style={styles.scannerIcon} />
                    <Text style={styles.scannerText}>
                      {I18n.t("scannerText")}
                    </Text>
                  </TouchableOpacity>
                </>
              }
              data={allServices}
              numColumns={2}
              keyExtractor={(item, index) => index}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    style={[styles.scannerimg, styles.shadow]}
                    key={index}
                    onPress={() =>
                      this.navigateParticularScreen({ item, index })
                    }
                  >
                    <Image
                      source={item.serviceIcon}
                      style={styles.scannerIcon}
                    />
                    <Text style={styles.serviceText}>{item.serviceName}</Text>
                  </TouchableOpacity>
                );
              }}
              columnWrapperStyle={styles.flatlistWrapper}
              ListFooterComponent={
                <>
                  <CommonDeliverOrder
                    ServiceName={I18n.t("kioskService")}
                    onPress={this.getKioskService}
                  />
                  <CommonDeliverOrder
                    ServiceName={I18n.t("curbService")}
                    onPress={this.getCurbSideService}
                  />
                  <CommonDeliverOrder
                    ServiceName={I18n.t("counterService")}
                    onPress={this.getCounterService}
                  />
                  <CommonDeliverOrder
                    ServiceName={I18n.t("courierService")}
                    onPress={this.getCourierService}
                  />
                  <CommonDeliverOrder
                    ServiceName={I18n.t("bedsideService")}
                    onPress={this.getBedSideService}
                  />

                  <View style={styles.bottomView}>
                    <Image
                      source={PoweredByLogo}
                      style={styles.bottomContent}
                    />
                  </View>
                </>
              }
            />
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    cartRecords: (tokenData) => {
      dispatch(getCartRecords(tokenData));
    },
  };
};

export default connect(null, mapDispatchToProps)(Home);
