import React, { Component } from "react";
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from "react-native";
import { CommingSoon } from "../../assets";
import { RFontSize } from "../../utils";
import styles from "./TeleHealthStyles";

export class TeleHealth extends Component {
  render() {
    return (
      <>
        <SafeAreaView style={styles.container}>
          <Animatable.Image
            animation="fadeInDownBig"
            source={CommingSoon}
            style={{
              resizeMode: "contain",
              width: RFontSize(300),
              height: RFontSize(500),
            }}
          />
        </SafeAreaView>
      </>
    );
  }
}

export default TeleHealth;
