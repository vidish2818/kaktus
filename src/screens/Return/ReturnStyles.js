import {StyleSheet, Dimensions} from 'react-native';
import { Colors } from "../../theme";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.white_Color,
    }
});
