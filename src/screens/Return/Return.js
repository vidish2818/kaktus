import React, { Component } from "react";
import * as Animatable from "react-native-animatable";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from "react-native";
import { CommingSoon } from "../../assets";
import { RFontSize } from "../../utils";
import styles from './ReturnStyles'

export class Return extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    // const {scannedOrders} = this.props;
    // let displayData;
    // if (scannedOrders != undefined) {
    //   displayData = scannedOrders.scannedOrders;
    // }
    // console.log('scannedOrders', scannedOrders.scannedOrders);
    return (
      <>
        <SafeAreaView
          style={styles.container}
        >
          <Animatable.Image
            animation="fadeInDownBig"
            source={CommingSoon}
            style={{
              resizeMode: "contain",
              width: RFontSize(300),
              height: RFontSize(500),
            }}
          />
          {/* <Text>{displayData ? displayData : 'Hello'}</Text> */}
        </SafeAreaView>
      </>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     scannedOrders: state.OrderDetails,
//   };
// };

// export default connect(mapStateToProps, null)(Return);

export default Return;
