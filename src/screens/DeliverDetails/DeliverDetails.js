import React, { Component } from "react";
import styles from "./DeliverDetailsStyles";
import { Text, View, SafeAreaView, Image, ScrollView } from "react-native";
import {
  AllOrdersListBox,
  Commonbackground,
  CommonButton,
  CommonLoader,
} from "../../components";
import { Kaktus_Logo, CartIcon, PoweredByLogo } from "../../assets";
import { connect } from "react-redux";
import { getCartRecords } from "../../redux/Actions/getCartRecords";
import Toast from "react-native-simple-toast";
import enums from "../../enums/enums";
import AsyncStorage from "@react-native-community/async-storage";
import { BASE_API_URL, BASE_API_URL_UPDATE } from "../../config/config";

export class DeliverDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataShown: false,
      isLoading: false,
      userToken: "",
      stockcode: [],
    };
  }

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userToken: data });
  }

  createCartItems = async (cartItems) => {
    let cartItemsArray = [];
    try {
      if (
        cartItems &&
        cartItems.barcodeScannedData &&
        cartItems.barcodeScannedData.response &&
        cartItems.barcodeScannedData.response.order_details &&
        cartItems.barcodeScannedData.response.order_details.length &&
        cartItems.barcodeScannedData.response.order_details.length > 0
      ) {
        for (
          let i = 0;
          i < cartItems.barcodeScannedData.response.order_details.length;
          i++
        ) {
          const updateCartItems = {
            stockCode:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.StockCode,
            puCode:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.pucode,
            orderId:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.OrderId,
            boxId:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.BoxId,
            patientId:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.PatientId,
            typeId: enums.Out_for_Delivery,
            deliveryNotes:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.DeliveryInstruction,
            totalItems:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order &&
              cartItems.barcodeScannedData.response.order.ItemsInThisOrder,
            rxNo:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details[i] &&
              cartItems.barcodeScannedData.response.order_details[i].RxNo,
            drugName:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details &&
              cartItems.barcodeScannedData.response.order_details[i].DrugName,
            price:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details[i] &&
              cartItems.barcodeScannedData.response.order_details[i].Price,
            ndcCode:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details &&
              cartItems.barcodeScannedData.response.order_details[i].NDCCode,
            isConsultRequired:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details[i] &&
              cartItems.barcodeScannedData.response.order_details[i]
                .IsConsultRequired,
            prescriptionNumber:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.order_details[i] &&
              cartItems.barcodeScannedData.response.order_details[i]
                .PrescriptionNumber,
            total:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.amount &&
              cartItems.barcodeScannedData.response.amount.Total,
            paid:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.amount &&
              cartItems.barcodeScannedData.response.amount.Paid,
            due:
              cartItems &&
              cartItems.barcodeScannedData &&
              cartItems.barcodeScannedData.response &&
              cartItems.barcodeScannedData.response.amount &&
              cartItems.barcodeScannedData.response.amount.Due,
          };
          cartItemsArray.push(updateCartItems);
        }
      }
      return cartItemsArray;
    } catch (erro) {
      return error;
    }
  };

  addToCartData = async () => {
    const { AllOrders } = this.props;
    const { userToken, isLoading } = this.state;
    let token = userToken?.signInUserSession?.idToken?.jwtToken;
    let orderIds = [];
    orderIds.push(AllOrders.barcodeScannedData.response.order.OrderId);
    let update = AllOrders.barcodeScannedData.response;
    let updateStatus = {
      box_id: update.order.BoxId,
      customer_id: update.order.PatientId,
      description: `Kaktus Pharmacy: Prescription status is updated to OUT FOR DELIVERY for pickup code ${update.order.pucode}`,
      event_name: "INSERT",
      event_type_name: "OUT_FOR_DELIVERY",
      location_notes: "",
      location_type_id: update.patient.location.Id,
      object_id: enums.Out_for_Delivery,
      order_id: orderIds,
      pickup_date: null,
      tofor_id: update.order.OrderId,
      type_id: enums.Out_for_Delivery,
    };

    fetch(`${BASE_API_URL_UPDATE}/deliveryconsole/updatestatus`, {
      method: "POST",
      withCredentials: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updateStatus),
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        this.props.navigation.goBack();
      });

    const CartItem = await this.createCartItems(AllOrders);
    const obj = {
      ordersDetails: CartItem,
    };
    this.setState({ isLoading: true });
    fetch(`${BASE_API_URL}/order-by-code/add-cart`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        if (res.success === true) {
          Toast.show(res.message, Toast.LONG);
          this.setState({ isLoading: false });
          setTimeout(() => {
            this.props.cartRecords(token);
          }, 5000);
        } else {
          Toast.show(res.message, Toast.LONG);
        }
      });
  };

  openAccordian = () => {
    const { dataShown } = this.state;
    this.setState({
      dataShown: !dataShown,
    });
  };

  render() {
    const { dataShown, isLoading } = this.state;
    const {
      order,
      order_details,
      patient,
      amount,
    } = this.props.AllOrders.barcodeScannedData.response;
    const { cartsData } = this.props;
    return (
      <>
        <CommonLoader
          loading={cartsData.isLoading ? cartsData.isLoading : isLoading}
        />
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            backPress={this.goToBackScreen}
          />
          <View style={styles.footerView}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
              style={{ flex: 1, paddingBottom: 400 }}
            >
              <View style={styles.headerView}>
                <Text style={styles.headerText}>{patient?.PatientName}</Text>
              </View>
              <AllOrdersListBox
                due={amount?.Due}
                paid={amount?.Paid}
                code={order?.StockCode}
                displayArrow={true}
                dataShow={dataShown}
                totalPrice={amount?.Total}
                listOfOrders={order_details}
                lengthOfItem={order?.ItemsInThisOrder}
                arrowOnpress={this.openAccordian}
                arrowWiseValue={!dataShown ? "Details" : "Minimize"}
              />
              <View style={styles.btnView}>
                <CommonButton
                  onPress={this.goToBackScreen}
                  btnName="Cancel"
                  textStyle={styles.btnText}
                  style={[styles.deliverBtn, styles.shadow]}
                />
                <CommonButton
                  btnName="Add to basket"
                  onPress={this.addToCartData}
                  textStyle={styles.btnText}
                  style={[styles.deliverBtn2, styles.shadow]}
                />
              </View>
              <View style={styles.bottomView}>
                <Image source={PoweredByLogo} style={styles.bottomContent} />
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    AllOrders: state.OrderDetails,
    cartsData: state.CartData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cartRecords: (tokenData) => {
      dispatch(getCartRecords(tokenData));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DeliverDetails);
