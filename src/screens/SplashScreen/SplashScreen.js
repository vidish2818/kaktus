import React, { Component } from "react";
import { View, Image, Easing, Animated, ImageBackground } from "react-native";
import * as Animatable from "react-native-animatable";
import { RFontSize } from "../../utils";
import { SplashScreen, SplashFooter, KaktusLogoSS } from "../../assets";
import AsyncStorage from "@react-native-community/async-storage";

export class Splashscreen extends Component {
  constructor(props) {
    super(props);
  }

  hideSplash = () => {
    AsyncStorage.getItem("user").then((res) => {
      if (res != null) {
        this.setState({
          isLogin: true,
        });
        this.props.navigation.replace("AppStack");
      } else if (res === null) {
        this.setState({
          isLogin: false,
        });
        this.props.navigation.replace("GetStarted");
      }
    });
  };
  componentDidMount() {
    setTimeout(() => {
      this.hideSplash();
    }, 2000);
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={SplashScreen}
          style={{ width: "100%", height: "100%" }}
        >
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Animatable.Image
              animation="fadeInDownBig"
              duration={1500}
              source={KaktusLogoSS}
              style={[
                {
                  width: RFontSize(293),
                  height: RFontSize(96),
                },
              ]}
            />
          </View>
          <Animatable.View
            animation="fadeInUp"
            style={{
              justifyContent: "center",
              flexDirection: "row",
              marginBottom: "10%",
            }}
          >
            <Image source={SplashFooter} />
          </Animatable.View>
        </ImageBackground>
      </View>
    );
  }
}

export default Splashscreen;
