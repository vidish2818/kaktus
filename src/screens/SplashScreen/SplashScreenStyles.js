import { StyleSheet, Dimensions } from "react-native";
import { Colors } from "../../theme";
import { Fonts } from "../../constant";
import { RFontSize } from "../../utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
});
