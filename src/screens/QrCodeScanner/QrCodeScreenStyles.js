import { StyleSheet, Dimensions } from "react-native";
import { Fonts, FontSize } from "../../constant";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";

const { height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary_Color,
  },
  headerSection: {
    flex: 1.0,
  },
  footerView: {
    flex: 5.0,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.FooterColor,
  },
  headerView: {
    width: "90%",
    marginTop: "10%",
    alignSelf: "center",
  },
  headerText: {
    fontSize: FontSize.font20,
    color: Colors.Black_Color,
    fontFamily: Fonts.fontSemiBold,
  },
  scannerView: {
    width: "auto",
    height: "auto",
    marginTop: "10%",
    flexWrap: "wrap",
  },
  bottomView: {
    marginTop: height > 910 ? "8%" : "15%",
    alignSelf: "center",
  },
  bottomContent: {
    width: RFontSize(152),
    height: RFontSize(28),
    resizeMode: "contain",
  },
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  deliverBtn: {
    width: "50%",
    marginTop: "10%",
    marginBottom: "10%",
    borderRadius: 50,
    alignSelf: "center",
    backgroundColor: Colors.Primary_Color,
  },
  btnText: {
    textAlign: "center",
    fontSize: FontSize.font13,
    color: Colors.white_Color,
    fontFamily: Fonts.fontMedium,
  },
  Qrcodeinputs: {
    width: "80%",
    marginTop: "8%",
    alignSelf: "center",
  },
  codeinput: {
    borderRadius: 10,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: Colors.BorderColor,
    textAlign: 'center'
  },
  commonInvalidQr: {
    width: "100%",
    paddingVertical: 30,
    borderBottomWidth: 0.5,
  },
  errormsg: {
    color: Colors.ErrorColor,
    fontSize: FontSize.font12,
    fontFamily: Fonts.fontRegular,
  },
});
