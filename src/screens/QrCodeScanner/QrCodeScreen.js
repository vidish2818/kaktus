import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import { I18n } from "../../constant";
import styles from "./QrCodeScreenStyles";
import {
  Commonbackground,
  CommonButton,
  CommonInput,
  CommonLoader,
} from "../../components";
import RBSheet from "react-native-raw-bottom-sheet";
import { CartIcon, PoweredByLogo, Kaktus_Logo } from "../../assets";
import QRCodeScanner from "react-native-qrcode-scanner";
import { allOrdersData } from "../../redux/Actions/allOrdersData.action";
import { ForwardArrow } from "../../assets/index";
import { RNCamera } from "react-native-camera";
import { Colors } from "../../theme";
import AsyncStorage from "@react-native-community/async-storage";

const { height, width } = Dimensions.get("window");

const InvalidQrSheet = (props) => {
  const { onPress } = props;
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.commonInvalidQr}>
        <Text style={{ textAlign: "center" }}>
          {" "}
          Invalid qrcode. Please try again
        </Text>
      </View>
      <View>
        <CommonButton
          onPress={onPress}
          textStyle={styles.btnText}
          style={[styles.deliverBtn, styles.shadow]}
          btnName="Cancel"
        />
      </View>
    </View>
  );
};

class QrCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scanCode: false,
      result: "",
      flashon: false,
      show: false,
      stockcode: "",
      scannedCode: "",
      isLoading: true,
      errors: {},
      userToken: ""
    };
  }

  inputChangeHandler = (name) => (event) => {
    this.setState({
      [name]: event,
    });
  };

  // stockCodeValidation = () => {
  //   const { Stockcode } = this.state;
  //   let errors = {};
  //   let IsValid = true;

  //   if (!Stockcode) {
  //     IsValid = false;
  //     errors["Stockcode"] = "please enter Stockcode";
  //   } else if (this.state.Stockcode.length < 6) {
  //     IsValid = false;
  //     errors["Stockcode"] = "stockcode must be atleast 6 characters";
  //   } else {
  //     null;
  //   }

  //   this.setState({
  //     errors: errors,
  //   });

  //   return IsValid;
  // };

  cancelOrder = () => {
    this.RBSheet.close();
  };

  async componentDidMount() {
    const data = JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ userToken: data });
  }

  onSuccess = (e) => {
    const { stockcode, userToken } = this.state;
    let token = userToken?.signInUserSession?.idToken?.jwtToken;
    this.setState({
      stockcode: stockcode,
    });
    if (e.data && e.data != undefined) {
      e.data && e.data.length === 6
        ? this.props.scannedQrcodeData(e.data, token)
        : this.RBSheet.open();
    } else {
      stockcode && stockcode.length === 6
        ? this.props.scannedQrcodeData(stockcode, token)
        : this.RBSheet.open();
    }
  };

  goToBackScreen = () => {
    this.props.navigation.goBack();
  };
  
  render() {
    const { flashon, stockcode} = this.state;
    const { scannedResponse } = this.props;
    return (
      <>
        <CommonLoader loading={scannedResponse.isLoading} />
        <SafeAreaView style={styles.container}>
          <Commonbackground
            BackarrowVisible={true}
            CartIconVisible={true}
            cartBtn={CartIcon}
            mainLogo={Kaktus_Logo}
            style={styles.headerSection}
            backPress={this.goToBackScreen}
          />
          <View style={[styles.footerView]}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1, paddingBottom: 200 }}
              style={{ flex: 1, paddingBottom: 1000 }}
            >
              <View style={styles.headerView}>
                <Text style={styles.headerText}>{I18n.t("scanScreen")}</Text>
              </View>
              <View style={styles.Qrcodeinputs}>
                <CommonInput
                  maxLength={6}
                  value={stockcode}
                  lockVisible={true}
                  userVisible={false}
                  eyeIcon={ForwardArrow}
                  eyeOnpress={this.onSuccess}
                  inputStyle={styles.codeinput}
                  placeholder={"Enter Stock Code"}
                  placeholderTextColor={Colors.Black_Color}
                  onChangeText={this.inputChangeHandler("stockcode")}
                />
                {/* {errors.Stockcode != undefined && (
                  <Text style={styles.errormsg}>{errors.Stockcode}</Text>
                )} */}
              </View>

              <View style={styles.scannerView}>
                <QRCodeScanner
                  showMarker={true}
                  reactivate={true}
                  reactivateTimeout={10}
                  onRead={this.onSuccess}
                  flashMode={
                    flashon ? RNCamera.Constants.FlashMode.torch : null
                  }
                  cameraStyle={{
                    width: width > 500 ? "100%" : 320,
                    alignSelf: "center",
                  }}
                  // cameraStyle={{ width: 320, alignSelf: "center", height: 350 }}
                  markerStyle={{ borderColor: "#fff", borderRadius: 10 }}
                  permissionDialogMessage="Need Permission To Access Camera"
                />
              </View>
              <View style={styles.bottomView}>
                <Image source={PoweredByLogo} />
              </View>
              <RBSheet
                ref={(ref) => {
                  this.RBSheet = ref;
                }}
                height={200}
                openDuration={250}
                customStyles={{
                  container: {
                    width: "100%",
                  },
                }}
              >
                <InvalidQrSheet onPress={this.cancelOrder} />
              </RBSheet>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    scannedResponse: state.OrderDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    scannedQrcodeData: (data, data1) => {
      dispatch(allOrdersData(data, data1));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QrCodeScreen);
