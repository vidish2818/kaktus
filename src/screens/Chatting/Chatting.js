import React, { Component } from "react";
import * as Animatable from "react-native-animatable";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from "react-native";
import styles from './ChattingStyles';
import { CommingSoon } from "../../assets";

export class Chatting extends Component {
  render() {
    return (
      <>
        <SafeAreaView
          style={styles.container}
        >
          <Animatable.Image
            animation="fadeInDownBig"
            source={CommingSoon}
            style={styles.animatedview}
          />
        </SafeAreaView>
      </>
    );
  }
}

export default Chatting;
