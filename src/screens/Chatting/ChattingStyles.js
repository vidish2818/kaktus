import { StyleSheet } from "react-native";
import { Colors } from "../../theme";
import { RFontSize } from "../../utils";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.white_Color,
  },
  animatedview: {
    resizeMode: "contain",
    width: RFontSize(300),
    height: RFontSize(500),
  },
});
