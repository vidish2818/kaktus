import I18n from 'react-native-i18n';
I18n.fallbacks = true;
I18n.defaultLocale = 'en';
// I18n.locale = 'gu';

export const en = {
  welcomeText: 'Welcome to kaktus Deliver',
  welcomeSubText: 'Expand your reach!',
  loginText: 'Login',
  signInWelcome: 'Welcome!',
  signInContinue: 'Sign in to continue',
  emailText: 'Email',
  pswText: 'Password',
  forgotText: 'Forgot Password',
  socialText: 'Or login with',
  workAt: "I'm working at",
  scannerText: 'Collect Orders',
  service1: 'Deliver Orders',
  service2: 'My Schedule',
  service3: 'Virtual Visit',
  service4: 'Chat',
  kioskService: 'Kiosk',
  curbService: 'Curb Side',
  counterService: 'Counter',
  courierService: 'Courier',
  bedsideService: 'Bed Side',
  seeOrder: 'View Order',
  tab1: 'Home',
  tab2: 'Deliver',
  tab3: 'Return',
  tab4: 'Telehealth',
  tab5: 'Chat',
  tab6: 'Setting',
  scanScreen: 'Scan Code',
  readyForDeliver: 'Orders Ready to Deliver',
  PendingOrders: 'Pending Orders',
  signatureScreen: 'Signature'
};

I18n.translations = {
  en,
};

export default I18n;
