import {Fonts, FontSize} from './font';
import I18n from './i18n';

export {Fonts, FontSize, I18n};
