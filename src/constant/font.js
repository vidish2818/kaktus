import {RFontSize} from '../utils';

export const Fonts = {
  fontMedium: 'Poppins-Medium',
  fontRegular: 'Poppins-Regular',
  fontSemiBold: 'Poppins-SemiBold',
};

export const FontSize = {
  font10: RFontSize(14),
  font12: RFontSize(14),
  font13: RFontSize(16),
  font14: RFontSize(17),
  font16: RFontSize(19),
  font20: RFontSize(22),
  font22: RFontSize(25),
  font24: RFontSize(26),
  font30: RFontSize(32),
};
