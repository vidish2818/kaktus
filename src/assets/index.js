export const Graphics = require('./images/Graphics.png');
export const Bg_Img = require('./images/Bg_Img.png');
export const Logo_Bg = require('./images/Logo_Bg.png');
export const Kaktus_Logo = require('./images/Kaktus_Logo.png');
export const Round_Bg = require('./images/Round_Bg.png');
export const Backarrow = require('./images/Backarrow.png');

export const UserLogo = require('./images/UserLogo.png');
export const LockIcon = require('./images/LockIcon.png');
export const CartIcon = require('./images/CartIcon.png');

export const EyeOn = require('./images/EyeOn.png');
export const EyeOff = require('./images/eye-slash.png');

export const Facebook = require('./images/Facebook.png');
export const Google = require('./images/Google.png');
export const Okta = require('./images/Okta.png');

export const PoweredByLogo = require('./images/PoweredByLogo.png');

export const HomeBottomIcon = require('./images/HomeBottomIcon.png');
export const DeliverBottomIcon = require('./images/DeliverBottomIcon.png');
export const ReturnBottomIcon = require('./images/ReturnBottomIcon.png');
export const TeleHealthBottomIcon = require('./images/TeleHealthBottomIcon.png');
export const ChatBottomIcon = require('./images/ChatBottomIcon.png');
export const SettingBottomIcon = require('./images/SettingBottomIcon.png');

export const DeactivateHome = require('./images/home.png');
export const DeactivateDeliver = require('./images/deliver.png');
export const DeactivateReturn = require('./images/return.png');
export const DeactivateTeleHealth = require('./images/telehealth.png');
export const DeactivateChat = require('./images/chat.png');
export const DeactivateSetting = require('./images/settting.png');

export const UserPofileImage = require('./images/UserPofileImage.png');
export const Scanner = require('./images/Scanner.png');
export const DeliverService = require('./images/DeliverService.png');
export const ReturnService = require('./images/ReturnService.png');
export const ChatService = require('./images/ChatService.png');
export const VirtualVisitService = require('./images/VirtualVisitService.png');
export const ForwardArrow = require('./images/ForwardArrow.png');
export const DownArrow = require('./images/DownArrow.png');
export const UpArrow = require('./images/UpArrow.png');

// Splash - Screen Images
export const SplashScreen = require('./images/SplashScreen.png');
export const SplashFooter = require('./images/SplashFooter.png');
export const KaktusLogoSS = require('./images/KaktusLogoSS.png');

export const GetStartedRoundBg = require('./images/GetStartedRoundBg.png');
export const CommingSoon = require('./images/CommingSoon.png');
