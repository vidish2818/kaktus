// import {RFontSize} from './index';
// import React, {Component} from 'react';
// import {KaktusLogoSS} from '../assets/index';
// import {Text, View, Animated, Easing} from 'react-native';

// export class AnimatedLoader extends Component {
//   constructor(props) {
//     super(props);
//     this.spinValue = new Animated.Value(0);
//     this.state = {};
//   }

//   spin = () => {
//     this.spinValue.setValue(0);
//     Animated.timing(this.spinValue, {
//       toValue: 2,
//       duration: 4000,
//       easing: Easing.linear,
//     }).start();
//   };

//   componentDidMount() {
//     this.spin();
//   }

//   render() {
//     const spin = this.spinValue.interpolate({
//       inputRange: [0, 1],
//       outputRange: ['0deg', '360deg'],
//     });
//     return (
//       <Modal
//         transparent={true}
//         animationType={'none'}
//         visible={loading}
//         onRequestClose={() => {
//           console.log('close modal');
//         }}>
//         <View style={styles.modalBackground}>
//           <View style={styles.activityIndicatorWrapper}>
//             <Animated.Image
//               style={[
//                 {
//                   width: RFontSize(293),
//                   height: RFontSize(96),
//                   transform: [{rotate: spin}],
//                 },
//               ]}
//               source={KaktusLogoSS}
//             />
//             <Text style={styles.loading_txt}>Loading...</Text>
//           </View>
//         </View>
//       </Modal>
//     );
//   }
// }

// export default AnimatedLoader;
