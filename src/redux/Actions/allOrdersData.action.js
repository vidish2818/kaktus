import axios from "axios";
import { BASE_API_URL } from "../../config/config";
import { actionTypes } from "./actionTypes";
import Toast from "react-native-simple-toast";
import { NavigationService } from "../../navigation/index";

const scancodeType = actionTypes("SCANNED_ORDERS_DATA");

export const allOrdersData = (userData, token) => {
  const stockCode = userData;

  return (dispatch) => {
    dispatch({
      type: scancodeType.REQUEST,
    });

    axios({
      url: `${BASE_API_URL}/order-by-code?code=${stockCode}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch({
            type: scancodeType.SUCCESS,
            payload: { data: res.data },
          });
          Toast.show(res.data.message, Toast.LONG);
          NavigationService.navigate("DeliveryDetails");
        } else {
          Toast.show(res.message, Toast.LONG);
          dispatch({
            type: scancodeType.FAILURE,
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
        if (err && err.response) {
          Toast.show(err.response.data.message, Toast.LONG);
        }
        dispatch({
          type: scancodeType.FAILURE,
        });
      });
  };
};
