import axios from "axios";
import { BASE_API_URL } from "../../config/config";
import { actionTypes } from "./actionTypes";

const cartDataType = actionTypes("GET_CART_RECORDS");

export const getCartRecords = (token) => {
  return (dispatch) => {
    dispatch({
      type: cartDataType.REQUEST,
    });
    axios({
        url: `${BASE_API_URL}/order-by-code/get-cart`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch({
            type: cartDataType.SUCCESS,
            payload: {data: res.data.results},
          });
        } else {
          dispatch({
            type: cartDataType.FAILURE,
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
        dispatch({
          type: cartDataType.FAILURE,
        });
      });
  }
}

