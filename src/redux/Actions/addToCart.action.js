const cartDataType = "CART_SAVED_DATA";
const removeFromCart = "REMOVE_FROM_CART"

export const addToCart = (orders) => (dispatch) => {
  dispatch({
    type: cartDataType,
    payload: orders,
  });
};

export const removeItem = (records) => dispatch => {
  dispatch({
      type: removeFromCart,
      payload: records
  })
}
