import { actionTypes } from "../Actions/actionTypes";

const initialState = {
  isLoading: false,
  allCartItems: [],
};

const cartDataType = actionTypes("GET_CART_RECORDS");

const CartItems = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case cartDataType.REQUEST:
      return { ...state, isLoading: true };
    case cartDataType.SUCCESS:
      return { ...state, allCartItems: payload.data, isLoading: false };
    case cartDataType.FAILURE:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default CartItems;
