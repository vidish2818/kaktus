    import { actionTypes } from "../Actions/actionTypes";

    const initialState = {
      isLoading: false,
      barcodeScannedData: [],
    };

    const scancodeType = actionTypes("SCANNED_ORDERS_DATA");

    const OrderDetails = (state = initialState, action) => {
      const { type, payload } = action;
  
      switch (type) {
        case scancodeType.REQUEST:
          return { ...state, isLoading: true };
        case scancodeType.SUCCESS:
          return { ...state, barcodeScannedData: payload.data, isLoading: false };
        case scancodeType.FAILURE:
          return { ...state, isLoading: false };
        default:
          return state;
      }
    };

    export default OrderDetails;
