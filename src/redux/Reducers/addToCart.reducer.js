import { actionTypes } from "../Actions/actionTypes";

const initialState = {
  isLoading: false,
  cartRecords: [],
  message: "Order successfully added into the cart!",
};

const cartDataType = "CART_SAVED_DATA";
const removeFromCart = "REMOVE_FROM_CART";

const CartData = (state = initialState, action) => {
  const { type, payload } = action;
  const { cartRecords } = state;
  switch (type) {
    case cartDataType:
      return {
        ...state,
        cartRecords: [...state.cartRecords, payload],
        isLoading: false,
      };
    case removeFromCart:
      return {
        ...state,
        cartRecords: cartRecords.filter(
          (items) =>
            items?.barcodeScannedData?.response?.order?.StockCode !==
            payload?.data?.barcodeScannedData?.response?.order?.StockCode
        ),
      };
    default:
      return state;
  }
};

export default CartData;
