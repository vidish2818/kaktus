import { combineReducers } from "redux";
import OrderDetails from "./allOrdersData.reducer";
import CartData from "./addToCart.reducer";
import AsyncStorage from "@react-native-community/async-storage";
import { persistReducer } from "redux-persist";
import CartItems from './getCartRecords.reducer'

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  cartDetils: ["cartRecords"],
};

const rootReducer = combineReducers({
  OrderDetails: OrderDetails,
  CartItems:CartItems,
  CartData: persistReducer(persistConfig, CartData),
});
export default rootReducer;
