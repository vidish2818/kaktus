import Amplify, { Auth } from 'aws-amplify';
import MyAsyncStorage from '@react-native-community/async-storage';

Amplify.configure({
    Auth: {
        region: 'us-east-1',
        userPoolId: 'us-east-1_GfjbrhU4b',
        userPoolWebClientId: '5g3rlf74nlca4d9b6dt64iv5e',
        mandatorySignIn: true,
        AsyncStorage: MyAsyncStorage,
    }
});

const currentConfig = Auth.configure();