import React, {Component} from 'react';
import Container from './src/Container';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './aws-config';
Amplify.configure(awsconfig);

export class App extends Component {
  render() {
    console.disableYellowBox = true
    return (
      <>
        <Container />
      </>
    );
  }
}

export default App;
